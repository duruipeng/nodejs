import logo from "./logo.svg";
import "./App.css";

import React, { useRef, useState } from "react";
// Import Swiper React components
// import { Swiper, SwiperSlide } from "swiper/react";
import { Swiper } from "./react/swiper";
import { SwiperSlide } from "./react/swiper-slide";
// import SwiperCore, { Pagination, Navigation } from "./build/swiper.cjs";
// import SwiperCore, { Pagination, Navigation } from "./build/swiper.cjs";
// import SwiperCore, { Pagination, Navigation } from "./build/swiper.cjs";

// Import Swiper styles
// import "/swiper.min.css";
// import "omponents/pagination/pagination.css";
// import "components/navigation/navigation.css";
// import "./build/swiper.min.css";
import "./components/pagination/pagination.less";
import "./components/navigation/navigation.less";

import "./index.css";

// import Swiper core and required modules
// import SwiperCore, { Pagination, Navigation } from "swiper/core";
import SwiperCore from "swiper/core";
import Pagination from "./components/pagination/pagination.js";
import Navigation from "./components/navigation/navigation.js";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);
const fruits = [
  ["2021/08/11 01:01:01", "title1", "title2", "title3", "title4", "title5"],
  ["2021/08/12 01:01:01", "head1", "head2", "head3", "head4", "head5"],
  ["2021/08/13 01:01:01", "body1", "body2", "body3", "body4", "body5"],
  ["2021/08/14 01:01:01", "text1", "text2", "text3", "text4", "text5"],
  ["2021/08/15 01:01:01", "head1", "head2", "head3", "head4", "head5"],
  ["2021/08/16 01:01:01", "body1", "body2", "body3", "body4", "body5"],
  ["2021/08/17 01:01:01", "text1", "text2", "text3", "text4", "text5"],
  ["2021/08/18 01:01:01", "head1", "head2", "head3", "head4", "head5"],
  ["2021/08/19 01:01:01", "body1", "body2", "body3", "body4", "body5"],
  ["2021/08/20 01:01:01", "text1", "text2", "text3", "text4", "text5"],
  ["2021/08/21 01:01:01", "head1", "head2", "head3", "head4", "head5"],
  ["2021/08/22 01:01:01", "body1", "body2", "body3", "body4", "body5"],
  ["2021/08/23 01:01:01", "text1", "text2", "text3", "text4", "text5"],
  ["2021/08/24 01:01:01", "head1", "head2", "head3", "head4", "head5"],
  ["2021/08/25 01:01:01", "body1", "body2", "body3", "body4", "body5"],
  ["2021/08/26 01:01:01", "text1", "text2", "text3", "text4", "text5"],
  ["2021/08/27 01:01:01", "footer1", "footer2", "footer3", "footer4", "footer5"]
];
export default function App() {
  const today = new Date();
  const year = today.getFullYear();
  const date = today.getDate();
  const month = today.getMonth();
  return (
    <>
      <Swiper
        slidesPerView={10}
        spaceBetween={30}
        slidesPerGroup={3}
        loop={true}
        loopFillGroupWithBlank={true}
        pagination={{
          clickable: false
        }}
        navigation={true}
        className="mySwiper"
      >
        {fruits.map((each, index) => {
          const current = new Date(each[0]);
          let isColor =
            current.getFullYear() === year &&
            current.getMonth() === month &&
            current.getDate() === date;
          return (
            <SwiperSlide style={{ background: isColor ? "#5234" : "" }}>
              <div key={index + "0"} className="each-slide">
                <p>{each[0]}</p>
              </div>
              <div key={index + "1"} className="each-slide">
                <p>{each[1]}</p>
              </div>
              <div key={index + "2"} className="each-slide">
                <p>{each[2]}</p>
              </div>
              <div key={index + "3"} className="each-slide">
                <p>{each[3]}</p>
              </div>
              <div key={index + "4"} className="each-slide">
                <p>{each[4]}</p>
              </div>
              <div key={index + "5"} className="each-slide">
                <p>{each[5]}</p>
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </>
  );
}
