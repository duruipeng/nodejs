import { makeStyles, fade } from "@material-ui/core/styles";

Export const useStyles = makeStyles(theme => ({
  Root: {
    Display: "flex",
    fontSize: 10,
    maxWidth: '100vw'
  },

  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),

    appBarShift: {
      zIndex: theme.zIndex.drawer + 1,
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.durations.enteringScreen
    },

    Button: {
      marginLeft: 5,
      marginRigh: 5
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
      background: '#fff'
    }
  }
}));