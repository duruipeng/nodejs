import Table from "./tableJSON.js";
import Header from "./header.js";
import Footer from "./footer.js";
import Home from "./home.js";

export { Table, Header, Footer };
