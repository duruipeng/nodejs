import React, { useState, useEffect } from 'react';
import MaterialTable, { MTableToolbar } from 'material-table';
import tableData from '../data/tableData.json';
import AlertDialog from '../components/AlertDialog';
import AlertDialog2 from '../components/AlertDialog2';
import AlertDialog3 from '../components/AlertDialog3';
import AlertDialog4 from '../components/AlertDialog4';
import '../App.css';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import BorderLinearProgress from './progressBar';
import { Tooltip, Typography } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import { Refresh } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  // root: {
  //   borderTop: theme.tables.borderSize,
  //   borderTopColor: theme.palette.primary.light,
  //   borderTopStyle: "solid"
  // },
  padding: {
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  tableRow: {
    '&:hover': {
      backgroundColor: '#999999 !important',
    },
  },
}));

export default function Table() {
  const [selectedRow, setSelectedRow] = useState([]);
  const [selectedRowNew, setSelectedRowNew] = useState([]);
  const [add, setAdd] = useState(0);
  const [numRows, setNumRows] = useState(10);

  // const Table = () => {
  // const [tableData, setTableData] = useState(fakeTableData);
  const classes = useStyles();

  //functions
  const title = 'Results';
  const header = [
    {
      title: 'ID',
      field: 'i_d',
    },
    {
      title: 'Operating system',
      field: 'o_s',
    },
    // Can use d3 to render element in the dom and set coordinates to fil or do a
    // dynamic cell fill if hex colour this set colour to that hex value and target
    //that cell on row/collumn
    {
      title: 'RGB',
      //   field: "rgb"
      // },
      render: data =>
        data ? (
          <BorderLinearProgress
            variant="determinate"
            value={data.rgb}
          />
        ) : (
          ''
        ),
      initialEditValue: 0,
    },

    {
      title: 'Hex',
      field: 'hex',
    },
    {
      title: 'Subnet Mask',
      field: 'subnet_Mask',
    },
    {
      title: 'MAC owner',
      field: 'mac_Owner',
    },
    // data: [
    //   { rgb: '#000000'  }
    // ],
  ];
  useEffect(() => {
    setSelectedRowNew(selectedRow);
  }, [selectedRow]);
  useEffect(() => {
    if (add === 2) {
      console.log(selectedRow);
      //tableData.push(selectedRowNew);
    }
    setAdd(1);
  }, [add, selectedRow]);
  return (
    <>
      {/* Will put table container and paper */}
      {/*         style
     classes.root */}
      <div
        className={clsx(
          classes.padding,
          classes.display_row,
          classes.tableRow
        )}
      >
        {/* <div className="display_row"> */}
        <MaterialTable
          // Variables passed into the table
          //   emptyRowsWhenPaging: false,
          onRowClick={(evt, selectedRow) => {
            tableData.push(selectedRow);
          }}
          title={title}
          columns={header}
          data={tableData}
          options={{
            sorting: true,
            filtering: true,
            grouping: true,
            exportButton: true,
            search: true,
            pageSize: numRows,
            //for records delete
            selection: true,
            // pageSize: 10,
            // paging: false,
            pageSizeOptions: [10, 20, 50],
            headerStyle: {
              backgroundColor: '#4DD0E1',
              color: '#FFF',
            },
            // rowStyle: (rowData) => ({
            //   backgroundColor:
            //     selectedRow && selectedRow.tableData.id === rowData.tableData.id
            //       ? "#EEE"
            //       : "#FFF"
            // })
          }}
          // Need to work on state to remove existing data
          actions={[
            {
              tooltip: 'Remove all selected records?',
              icon: 'delete',
              onClick: (evt, rowData) =>
                alert(
                  'Warning! You are about to delete ' +
                    rowData.length +
                    ' rows'
                ),
            },
          ]}
          components={{
            Toolbar: props => (
              <div>
                <Typography
                  component="h5"
                  variant="h6"
                  color="inherit"
                  noWrap
                  className="typography"
                >
                  {'Local mobile phone table'}
                </Typography>
                <Link to="/home" className="addButtonPlace">
                  <Tooltip title={'Refresh data'}>
                    <Refresh
                      fontSize="large"
                      className="addButtonIcon"
                      role="link"
                    />
                  </Tooltip>
                </Link>
                <MTableToolbar {...props} />
                <div style={{ clear: 'both' }} />
                <AlertDialog />
                <AlertDialog2 />
                <AlertDialog3 />
                <AlertDialog4 setAdd={setAdd} />
              </div>
            ),
          }}
        />
      </div>
    </>
  );
}
