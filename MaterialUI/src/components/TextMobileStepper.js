import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const innerNews = [
    {
        label: '20日　九州から東海で大雨続く　土砂災害に厳重警戒を',
        text:
            '九州から東北にかけて、大雨と長雨をもたらしている前線は、活動が弱まり、あす(20日)にかけて次第に不明瞭になる見通しです。ただ、西日本から東日本の太平洋側、特に、九州や四国、紀伊半島では、あすも局地的に激しい雨が降る予想です。これは、前線がはっきりしなくなっても、南からの暖かく湿った空気が流れ込むのと、日本の上空に冷たい空気があり、大気が非常に不安定な状態が続くためです。',
    },
    {
        label: '東京都で新たに5534人の感染確認 木曜日として最多 重症者は1人減の274人',
        text:
            '19日、東京都が確認した新型コロナウイルスの新たな感染者は5534人だった。13日の5773人に次ぐ過去2番目の多さで、木曜日としての最多を更新した。',
    },
    {
        label: '「感染拡大を最優先」発言を修正　首相会見録、HP上で',
        text:
            '加藤勝信官房長官は19日の記者会見で、首相官邸のホームページ上で菅義偉首相の17日夜の記者会見録を修正したと明らかにした。衆院選の時期を巡り「感染拡大を最優先に」とした首相の発言をそのまま18日に公開をしたが、指摘を受け同日中に「感染拡大の防止を最優先に」と変更したという。',
    },
    {
        label: '岸田氏に派閥議員が総裁選出馬要請　２６日以降に決断',
        text:
            '自民党岸田派（宏池会）は１９日、東京都内で会合を開き、菅義偉（すが・よしひで）首相の総裁任期満了（９月３０日）に伴う次期総裁選への対応を協議した。岸田派議員からは領袖（りょうしゅう）の岸田文雄前政調会長に出馬を求める声が相次いだ。総裁選をめぐっては党内で「９月１７日告示－２９日投開票」の日程が有力視されているが、岸田氏は今月２６日に日程が正式に決まり次第、最終判断する意向だ。',
    },
    {
        label: 'トヨタの販売会社９社、同意得ずに個人情報３３１８人分をＩＤ登録',
        text:
            'トヨタ自動車は１９日、販売会社９社で、計３３１８人分の個人情報を、顧客の同意を得ずに同社のネットサービスＩＤに登録していたと発表した。',
    },
];

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 550,
        flexGrow: 1,
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
    },
    pageBtn: {
        border: '1px solid #efefef',
        marginBottom: 5,
        borderRadius: 2,
        color: 'black',
        // padding: '1px 5px',
        height: '30px',
    },
    title: {
        display: 'flex',
        border: '1px solid #efefef',
        marginBottom: 5,
        borderRadius: 2,
        // margin: '0px 5px 0px 5px',
        padding: '0px 5px 0px 5px',
        height: '30px',
        width: '400px',
        alignItems: 'center',
    },
    textField: {
        //   background: 'linear-gradient(45deg, #ffffff, #efefef)',
        border: '1px solid #efefef',
        marginBottom: 5,
        borderRadius: 2,
        margin: '0px 8px',
        width: '534px',
        height: '100px',
        overflow: 'auto',
    }
}));

export default function TextMobileStepper(props) {
    let { news, dblclickEvent } = props;
    if (news == null) news = innerNews;
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = news.length;

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    return (
        <div className={classes.root}>
            <MobileStepper
                // steps={maxSteps}
                position="static"
                variant="none"
                activeStep={activeStep}
                style={{ background: 'none' }}
                nextButton={
                    <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1} className={classes.pageBtn}>
                        次へ<div style={{ color: activeStep === maxSteps - 1 ? '#efefef' : 'blue', fontSize: '18px' }}>▼</div>
                    </Button>
                }
                backButton={
                    <>
                        <Button size="small" onClick={handleBack} disabled={activeStep === 0} className={classes.pageBtn}>
                            {/* {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />} */}
                            <div style={{ color: activeStep === 0 ? '#efefef' : 'blue', fontSize: '18px' }}>▲</div>前へ
                        </Button>
                        <Paper square elevation={0} className={classes.title}>
                            <Typography>{news[activeStep].label.length > 20 ? news[activeStep].label.substr(0, 20) : news[activeStep].label}</Typography>
                        </Paper>
                    </>
                }
            />
            <Paper square elevation={0} className={classes.textField}>
                <Typography>{news[activeStep].text.length > 200 ? news[activeStep].text.substr(0, 200) : news[activeStep].text}</Typography>
            </Paper>
        </div>
    );
}