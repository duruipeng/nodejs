import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import SwipeableTextMobileStepper from '../components/SwipeableTextMobileStepper';
import TextMobileStepper from './TextMobileStepper';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import './AlertDialog3.scss';

function PaperComponent(props) {
  return (
    <Draggable
      handle=".dialogTitle"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

export default function AlertDialog() {
  const [open, setOpen] = React.useState(false);
  const [news, setNews] = React.useState();
  const [dblclickEvent, setDblclickEvent] = React.useState();
  const [isOn, setIsOn] = React.useState(false);
  const [btnName, setBtnName] = React.useState('');

  const useNormalStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 15,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    },
    rootActive: {
      background: 'linear-gradient(45deg, #ffffff, #a41414)',
      border: '1px solid #a41414',
      marginBottom: 5,
      borderRadius: 15,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    },
    topScrollPaper: {
      alignItems: 'flex-start',
    },
    topPaperScrollBody: {
      verticalAlign: 'top',
    },
  });
  const cancelBtnStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 1,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleConfirm = text => {
    setBtnName(text);
    setIsOn(true);
  };

  const ButtonStyled = props => {
    const { text, handleClose } = props;
    const nomalBtnClasses = useNormalStyles();
    const cancelBtnClasses = cancelBtnStyles();
    const nomalBtn = (
      <Button
        className={
          isOn && btnName === text
            ? nomalBtnClasses.rootActive
            : nomalBtnClasses.root
        }
        color="primary"
        onClick={() => {
          handleConfirm(text);
        }}
      >
        {text}
      </Button>
    );
    const cancelBtn = (
      <Button
        className={cancelBtnClasses.root}
        color="primary"
        onClick={handleClose}
      >
        {text}
      </Button>
    );
    if (text === 'キャンセル') {
      return cancelBtn;
    } else {
      return nomalBtn;
    }
  };
  useEffect(() => {
    window.addEventListener('dblclick', function(e) {});

    const el = document.getElementsByClassName(
      'MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft'
    );
    let newArr = Array.from(el);
    const val = newArr.map((item, index) => {
      const value = {
        index,
        label: index + '：' + item.innerText,
        text:
          index +
          '：' +
          item.innerText +
          '　' +
          item.offsetHeight +
          '　' +
          item.offsetLeft +
          '　' +
          item.offsetTop +
          '　' +
          item.offsetWidth,
        offsetTop: item.offsetTop,
      };
      return value;
    });
    setNews(val);

    const event = newArr.map((item, index) => {
      const ev = {
        index,
        event: item,
      };
      return ev;
    });
    setDblclickEvent(event);
  }, []);
  const handleBackdropClick = () => {
    const el = document.getElementById('searchBar');
    el.blur();
    console.log('el', el);
    el.focus();
    setTimeout(function() {}, 0);
  };
  const StyledDialog = withStyles({
    root: { pointerEvents: 'none' },
    paper: { pointerEvents: 'auto' },
  })(props => <Dialog hideBackdrop {...props} />);
  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
      >
        Open alert dialog3
      </Button>
      <Dialog
        open={open}
        // onClose={handleBackdropClick}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        style={{
          width: '100%',
          maxWidth: 'none',
          justifyContent: 'flex-end !important',
        }}
        PaperComponent={PaperComponent}
        classes={{
          scrollPaper: useNormalStyles.topScrollPaper,
          paperScrollBody: useNormalStyles.topPaperScrollBody,
        }}
        disableEnforceFocus
        style={{ pointerEvents: 'none' }}
        PaperProps={{ style: { pointerEvents: 'auto' } }}
      >
        <div className="dialogTitle" style={{ cursor: 'move' }}>
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </div>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means
            sending anonymous location data to Google, even when no
            apps are running.
          </DialogContentText>

          <SwipeableTextMobileStepper
            news={news}
            dblclickEvent={dblclickEvent}
          />
          <TextMobileStepper
            news={news}
            dblclickEvent={dblclickEvent}
          />

          {/* <TextField
            id="outlined-multiline-static"
            label="Multiline"
            multiline
            rows={4}
            defaultValue="Default Value"
            variant="outlined"
            style={{ width: '550px' }}
          /> */}
          <div className="dialogAdd">
            <div className="Add1">
              <div className="Addbtn" style={{ width: '150px' }}>
                <ButtonStyled text={'addaddaddadd'} />
              </div>
              <div className="Addbtn">
                <ButtonStyled text={'add1'} />
              </div>
            </div>
            <div className="Add2">
              <div className="Addbtn">
                <ButtonStyled text={'add2'} />
              </div>
              <div className="Addbtn">
                <ButtonStyled text={'add'} />
              </div>
              <div className="Addbtn">
                <ButtonStyled text={'add3'} />
              </div>
            </div>
            <div className="Add3">
              <div className="Addbtn" style={{ marginRight: 'auto' }}>
                <ButtonStyled
                  text={'キャンセル'}
                  handleClose={handleClose}
                />
              </div>
              <div className="Addbtn">
                <ButtonStyled text={'add4'} />
              </div>
              <div className="Addbtn">
                <ButtonStyled text={'add5'} />
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
