import React from "react";
import clsx from "clsx";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Grid, Tooltip } from "@material-ui/core";
import { lightGreen } from "@material-ui/core/colors";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGithub } from "@fortawesome/free-brands-svg-icons";

const useStyles = makeStyles(theme => ({
  bottomSection: {
    textAlign: "left",
    margin: "0px 0px 0px 0px",
    backgroundColor: "#3f51b5",
    minHeight: "120px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "0px 65px",
    flex: 1
    // position: "absolute",
    // display: "flex",
    // justifyContent: "center"
  },

  root: {
    minHeight: "2vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    marginTop: "auto"
  },
  footer: {
    width: "100%",
    padding: theme.spacing(2, 2)
  },
  footerContainer: {
    display: "flex",
    justifyContent: "center"
  },
  block: {
    padding: "32px 0",
    display: "flex",
    alignItems: "center",
    textAlign: "center",
    flexDirection: "column",
    borderBottom: "none",
    [theme.breakpoints.down("md")]: {
      borderBottom: "1px solid white"
    }
  },
  blockTitle: {
    color: "white",
    fontSize: "16px",
    fontWeight: "bold",
    lineHeight: "25px",
    marginBottom: "16px",
    textDecoration: "none",
    textTransform: "uppercase"
  },
  github: {
    "&:hover": {
      color: "#32CD32",
      justifyContent: "center"
    }
  },
  socialNetworkIcon: {
    margin: 5,
    color: "white",
    textDecoration: "none",
    anchor: "right"
  },
  link: {
    cursor: "pointer",
    lineHeight: "25px",
    color: "white",
    textDecoration: "underline",
    "&:hover": {
      color: lightGreen[400]
    }
  }
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />

      <Grid container className={classes.bottomSection}>
        <Grid item xs={12} sm={4} md={3} className={classes.bottomSection}>
          <Tooltip title="Email">
            <a
              className={classes.link}
              target="_blank"
              rel="noopener noreferrer"
              href="mailto:helper@help.com?subject=[Contact]"
              role="link"
              aria-label="Contact email"
            >
              Contact me{" "}
            </a>
          </Tooltip>
        </Grid>

        <div>
          <Tooltip title="React Github">
            <a
              className={clsx(classes.socialNetworkIcon, classes.github)}
              href="https://github.com/facebook/react"
              target="_blank"
              rel="noopener noreferrer"
              role="link"
              aria-label="React Github link"
            >
              <FontAwesomeIcon size="3x" icon={faGithub} />
            </a>
          </Tooltip>
        </div>
      </Grid>

      <footer className={classes.footer}>
        <Container className={classes.footerContainer}>
          <Typography
            variant="body2"
            color="textSecondary"
            role="textbox"
            aria-label="Current year"
          >
            2020
          </Typography>
        </Container>
      </footer>
    </div>
  );
}
