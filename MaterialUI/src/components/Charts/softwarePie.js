import { Chart } from "chart.js";
// import tableData from ".../data/tableData";
import tableData from "../../data/tableData.json";

export default function Pie2(setData) {
  var ctx = document.querySelector("#pie").getContext("2d");
  new Chart(ctx, {
    type: "pie",
    data: {
      datasets: [
        {
          data:
            // [tableData],
            // Requires data to be a number
            // Perhaps assign a incremental number
            //to the values which can be dynamic totaled
            //might work?
            [3, 5, 1],

          backgroundColor: ["#EBEBE3", "#a4c639", "#00a2ed"],
          borderWidth: 3
        }
      ],
      labels: ["iOS", "Android", "Windows"]
    },
    options: {
      title: {
        display: true,
        fontFamily: "Roboto",
        text: "Pie chart of software from mobile phones",
        fontSize: 14
      },
      animation: {
        animateRotate: true
      }
    }
  });
}
