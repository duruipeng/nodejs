import React, { useEffect } from "react";
import { Card, CardContent } from "@material-ui/core";
import Pie from "./macPie";
import Pie2 from "./softwarePie";

function MacPie(getData) {
  useEffect(() => {
    Pie();
  }, []);

  return (
    <Card>
      <CardContent>
        <canvas id="pie" height="260" width="300" />
      </CardContent>
    </Card>
  );
}

function SoftwarePie(getData) {
  useEffect(() => {
    Pie2();
  }, []);

  return (
    <Card>
      <CardContent>
        <canvas id="pie2" height="260" width="300" />
      </CardContent>
    </Card>
  );
}

export { MacPie, SoftwarePie };
