import { Chart } from "chart.js";
// import tableData from ".../data/tableData";
import tableData from "../../data/tableData.json";

export default function Pie(setData) {
  var ctx = document.querySelector("#pie").getContext("2d");
  new Chart(ctx, {
    type: "pie",
    data: {
      datasets: [
        {
          data:
            // [tableData],
            // Requires data to be a number
            // Perhaps assign a incremental number
            //to the values which can be dynamic totaled
            //might work?
            [3, 5, 1, 2, 1, 1, 1],

          backgroundColor: [
            "#3b7a57",
            "#ed1c24 ",
            "#EBEBE3",
            "#f50057",
            "#000000",
            "#f80000",
            "#3b5998"
          ],
          borderWidth: 3
        }
      ],
      labels: [
        "Amazon",
        "Huawei",
        "Apple",
        "Google",
        "UNKNOWN",
        "Oracle",
        "Facebook"
        // mac_Owner
      ]
    },
    options: {
      title: {
        display: true,
        fontFamily: "Roboto",
        text: "Pie chart of MAC address owners from mobile phones",
        fontSize: 14
      },
      animation: {
        animateRotate: true
      }
    }
  });
}
