import React from "react";
import PropTypes from "prop-types";
import { ListItem, ListItemText, Paper, List, Grid } from "@material-ui/core";
import { Pie } from "react-chartjs-2";
import { Table } from "./index.js";

const Summary = props => {
  const { viewSummaryOnClick } = props;

  const globalData = {
    labels: ["iOS", "Android", "Windows"],
    datasets: [
      {
        data: [
          // state.mac
        ],
        backgroundColor: ["#78a7de", "#e38181", "#baedb4"],
        hoverBackgroundColor: ["#17a2b8", "#ad4b4b", "#28a745"]
      }
    ]
  };

  return (
    <Paper>
      <Grid item container spacing={6} style={{ padding: "100px" }}>
        <Grid item xs={8} />
        <Grid item xs={12}>
          <Table />
        </Grid>
      </Grid>

      <List>
        <ListItem button dense onClick={viewSummaryOnClick}>
          <ListItemText>
            <b> Summary</b>
          </ListItemText>
        </ListItem>
      </List>

      <Pie
        data={globalData}
        className="pie-chart"
        options={{
          responsive: true,
          maintainAspectRatio: true
        }}
        width="170"
      />
    </Paper>
  );
};

// viewSummaryOnClick.PropTypes = {
//   viewSummaryOnClick: PropTypes.func
// };

export default Summary;
