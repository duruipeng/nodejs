import React from "react";

const Error404 = () => {
  return (
    <div>
      <p style={{ textAlign: "center" }}>SORRY PAGE NOT FOUND</p>
    </div>
  );
};
export default Error404