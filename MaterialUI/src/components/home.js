import React from "react";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/styles';
import "../styles.css";
import {
  Button,
  Grid,
  Typography,
  Backdrop,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Tooltip
} from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Table } from "../components/index.js";
import { MacPie, SoftwarePie } from "../components/Charts/index";
import { Error404 } from "../components/error404.js";
import { ExpandMore } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
}));

function Home() {
  const classes = useStyles();

  //for backdrop
  // const [open, setOpen] = React.useState(false);
  // const handleClose = () => {
  //   setOpen(false);
  // };
  // const handleToggle = () => {
  //   setOpen(!open);
  // };

  return (
    <MuiThemeProvider>
      <Grid
        container
        style={{ padding: "1px" }}
        direction="column"
        className="App"
        role="application"
        aria-label="My project"
      >
        <Grid item container spacing={5} style={{ padding: "70px" }}>
          <Grid item xs={2} />
          <Grid item xs={8}>
            <Tooltip title="open">
              <ExpansionPanel role="button">
                <ExpansionPanelSummary
                  expandIcon={<ExpandMore />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className={classes.heading}>
                    How to use
                  </Typography>
                </ExpansionPanelSummary>

                <ExpansionPanelDetails>
                  {" "}
                  <Typography>
                    For more info check: https://material-table.com/#/ and
                    https://www.chartjs.org/
                  </Typography>{" "}
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Tooltip>
          </Grid>
        </Grid>

        <Grid item container spacing={2} style={{ padding: "1px" }}>
          <Grid item xs={8} />
          <Grid item xs={12}>
            <Table />
          </Grid>

          <Grid item container spacing={4} style={{ padding: "24px" }}>
            <Grid item xs={2} />
            <Grid item xs={8}>
              <Link to="Summary" style={{ textDecoration: "none" }}>
                <Button fullWidth variant="contained" color="primary">
                  View Summary
                </Button>
              </Link>

              <Grid item container spacing={4} style={{ padding: "24px" }}>
                <Grid item xs={2} />
                <Grid item xs={8}>
                  <div className="pie_Size">
                    <MacPie />
                  </div>
                </Grid>
              </Grid>

              {/* {/* <Grid item container spacing={4} style={{ padding: "24px" }}>
            <Grid item xs={2} />
            <Grid item xs={8}>
              <div className="pie_Size">
                <SoftwarePie />
              </div>
            </Grid>
          </Grid> */}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </MuiThemeProvider>
  );
}
export default Home;
