import React, { useState, useEffect } from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  Grid,
  Tooltip,
  IconButton,
  Box,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import {
  RadioButtonUnchecked,
  Menu,
  Notifications,
  Brightness2,
  WbSunnyIcon,
  Search,
} from '@material-ui/icons';
import { makeStyles, useTheme, fade } from '@material-ui/core/styles';
import TemporaryDrawer from './sideDraw';
import clsx from 'clsx';
import ScaleLoader from 'react-spinners/ScaleLoader';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import Clock from 'react-live-clock';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  darkModeButton: {
    position: 'absolute',
    right: theme.spacing(2),
    transform: 'rotate(-215deg)',
  },
  root: {
    flexGrow: 1,
    display: 'flex',
    color: 'white',
    // background: 'linear-gradient(to right, #614385, #516395)',
    // margin: '0 0 25px 0'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  typographyStyles: {
    flex: 4,
  },
  appBar: {
    backgroundColor: '#2196f3',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  placeholder: {
    fontSize: theme.typography.fontSize,
    backgroundColor: '#2196f3',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  appBarShift: {
    zIndex: theme.zIndex.drawer + 1,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
    background: '#ff5722',
  },
  input: {
    color: 'white',
  },
  // root: {
  //   background: "white"
  // },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    background: '#d1f4ff',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    background: '#18ffff',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
    background: '#ff5722',
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  text: {
    // fontSize: "1.2rem",
    // fontWeight: "900",
    // textTransform: "uppercase",
    // paddingLeft: "15px",
    // display: "flex",
    flexGrow: 1,
  },

  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    border: `1px solid ${theme.palette.accent}`,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    color: theme.palette.accent,
  },
}));

export default function Header() {
  // const Header = () => {
  //  const Headers = ({ onDarkModeChange, darkMode }) => {

  const classes = useStyles();

  const theme = useTheme();
  const [openDrawer, setOpendrawer] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpendrawer(true);
  };

  const handleDrawerClose = () => {
    setOpendrawer(false);
  };

  // const handleThemeToggle = this.handleThemeToggle;

  return (
    // <AppBar position="static" className={classes.root}>
    <AppBar
      aria-label="Header"
      role="navigation"
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: open,
      })}
    >
      <Toolbar>
        <IconButton
          aria-label="Open side menu"
          role="button"
          aria-label="opens drawer"
          color="inherit"
          onClick={handleDrawerOpen}
          edge="start"
          className={clsx(classes.menuButton, open && classes.hide)}
        >
          <Menu />
        </IconButton>
        <TemporaryDrawer
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
        />
        {/* <div className="Text"> */}

        {/* <Link> */}
        <Typography className={classes.title} variant="h6" noWrap>
          Mobile network device tracker
        </Typography>
        {/* <Link/> */}
        <Box>
          <TextField
            // defaultValue="color"
            id="searchBar"
            size="small"
            className={classes.root}
            variant="outlined"
            placeholder="Search for results..."
            fullWidth
            InputProps={{
              classes: {
                input: classes.placeholder,
                className: classes.input,
              },
              startAdornment: (
                <InputAdornment position="start">
                  <Search className={classes.searchIcon} />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <ScaleLoader height={20} margin={2} width={3} color="White" />
        {/* </div> */}
        {/* <Typography className={classes.typographyStyles}>
            {" "}
            Mobile network device tracker{" "}
          </Typography> */}
        {/* <Tooltip title="Wonderwheel">
            <IconButton aria-label="Wonderwheel" color="inherit">
              <RadioButtonUnchecked align="left" aria-label="Wonderwheel" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Notifications">
            <IconButton aria-label="Notifications" color="inherit">
              <Notifications align="left" aria-label="Notifications" />
            </IconButton>
          </Tooltip> */}

        {/* Outdated version*/}
        {/* 
        <IconButton onClick={handleThemeToggle}>
          {this.props.currentTheme === "light" ? (
            <Brightness2OutlinedIcon />
          ) : (
            <Brightness5OutlinedIcon />
          )}
        </IconButton> */}
        <Tooltip title="Current time">
          <Clock
            format={'HH:mm:ssa M/MM/YYYY'}
            ticking={true}
            timezone={'Asia/Tokyo'}
          />
        </Tooltip>
      </Toolbar>
    </AppBar>
  );
}
