import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export default function AlertDialog() {
  const [open, setOpen] = React.useState(false);
  const [isFocus, setIsFocus] = React.useState(false);

  const useNormalStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 15,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    }
  })
  const cancelBtnStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 1,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    }
  })

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const ButtonStyled = (props) => {
    const { text, handleClose } = props;
    const nomalBtnClasses = useNormalStyles();
    const cancelBtnClasses = cancelBtnStyles();
    const nomalBtn = <Button className={nomalBtnClasses.root} color="primary" onClick={()=>{setIsFocus(true)}}>{text}</Button>;
    const cancelBtn = <Button className={cancelBtnClasses.root} color="primary" onClick={handleClose}>{text}</Button>;
    if (text === 'キャンセル') {
      return cancelBtn
    } else {
      return nomalBtn
    }
  };
  useEffect(()=>{
    if(isFocus) {
      document.getElementsByTagName('textarea')[0].focus();
    }
  },[isFocus]);
  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open alert dialog
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        style={{ width: '100%', maxWidth: 'none' }}
      >
        <div className='dialogTitle'>
          <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
          <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
            <CloseIcon />
          </IconButton>
        </div>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending anonymous location data to
            Google, even when no apps are running.
          </DialogContentText>

          <TextField
            id="outlined-multiline-static"
            label="Multiline"
            multiline
            rows={4}
            defaultValue="Default Value"
            variant="outlined"
            style={{ width: '550px' }}
          />
          <div className='dialogAdd'>
            <div className='Add1'>
              <div className='Addbtn' style={{ width: '150px' }}>
                <ButtonStyled text={'addaddaddadd'} />
              </div>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
            </div>
            <div className='Add2'>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
            </div>
            <div className='Add3'>
              <div className='Addbtn' style={{ marginRight: 'auto' }}>
                <ButtonStyled text={'キャンセル'} handleClose={handleClose}/>
              </div>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
              <div className='Addbtn'>
                <ButtonStyled text={'add'} />
              </div>
            </div>
          </div>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}