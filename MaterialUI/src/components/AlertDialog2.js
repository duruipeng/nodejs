import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import './AlertDialog2.scss';

export default function AlertDialog2() {
  const [open, setOpen] = React.useState(false);

  const useNormalStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 15,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    },
  });
  const cancelBtnStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #ffffff, #efefef)',
      border: '1px solid #efefef',
      marginBottom: 5,
      borderRadius: 1,
      color: 'black',
      // padding: '1px 5px',
      height: '30px',
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const ButtonStyled = props => {
    const { text, handleClose } = props;
    const nomalBtnClasses = useNormalStyles();
    const cancelBtnClasses = cancelBtnStyles();
    const nomalBtn = (
      <Button
        className={nomalBtnClasses.root}
        color="primary"
        onClick={handleClose}
      >
        {text}
      </Button>
    );
    const cancelBtn = (
      <Button
        className={cancelBtnClasses.root}
        color="primary"
        onClick={handleClose}
      >
        {text}
      </Button>
    );
    if (text === 'キャンセル') {
      return cancelBtn;
    } else {
      return nomalBtn;
    }
  };
  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
      >
        Open alert dialog2
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        style={{ width: '100%', maxWidth: 'none' }}
      >
        <div className="dialogTitle">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </div>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means
            sending anonymous location data to Google, even when no
            apps are running.
          </DialogContentText>

          <div className="group-aef0dd70">
            <div className="rect-01a38f26"></div>
            <div className="editRoot">
              aaa
              <div className="rect-23433920"></div>
              <div className="text-absolute-e33c1008">
                <div className="grading_black_18dp-89af0668">
                  <div className="v-stack-d3d660b8"></div>
                  <div className="v-stack-74446818">
                    <div className="group-98aebb50">
                      <div className="rect-55a80e48"></div>
                      <div className="rect-a6a15098"></div>
                      <div className="rect-7499817c"></div>
                      <div className="rect-e2cb9a08"></div>
                      <div className="rect-378331e0"></div>
                      <div className="svg-372330c0"></div>
                    </div>
                  </div>
                </div>
                <div className="text-16bcd768">aaaaaadadrdaaaaa</div>
              </div>
              <div className="rect-0489d780">
                <div className="text-absolute-6da24c64">
                  <div className="grading_black_18dp-6a88704e">
                    <div className="v-stack-d3d660b8"></div>
                    <div className="v-stack-74446818">
                      <div className="group-98aebb50">
                        <div className="rect-55a80e48"></div>
                        <div className="rect-a6a15098"></div>
                        <div className="rect-7499817c"></div>
                        <div className="rect-e2cb9a08"></div>
                        <div className="rect-378331e0"></div>
                        <div className="svg-372330c0"></div>
                      </div>
                    </div>
                  </div>
                  <div className="text-16bcd768">aaa</div>
                </div>
              </div>
              <div className="rect-bc294398"></div>
              <div className="rect-23820adc"></div>
              <div className="rect-442d8aca"></div>
              <div className="rect-3c794582"></div>
              <div className="text-absolute-1d634390">
                <div className="text-16bcd768">aaaaaa</div>
              </div>
              <div className="text-4cc4f200">aaaaaa</div>
              <div className="text-fb94d700">aaaaaa</div>
              <div className="text-175754d2">aaaaaa</div>
              <div className="rect-4645a974_group">
                <div className="v-stack-d3d660b8_aa"></div>
                <div className="v-stack-74446818_aa">
                  <div className="group-98aebb50_aa">
                    <div className="rect-55a80e48_aa"></div>
                    <div className="rect-a6a15098_aa"></div>
                    <div className="rect-7499817c_aa"></div>
                    <div className="rect-e2cb9a08_aa"></div>
                    <div className="rect-378331e0_aa"></div>
                    <div className="svg-372330c0_aa"></div>
                  </div>
                </div>
                <div className="rect-4645a974"></div>
                <div className="text-240bc850">aaa</div>
              </div>
              <div className="rect-446041b4"></div>
              <div className="text-00508d84">aaa</div>
              <div className="grading_black_18dp-27501ec4">
                <div className="v-stack-d3d660b8"></div>
                <div className="v-stack-74446818">
                  <div className="group-98aebb50">
                    <div className="rect-55a80e48"></div>
                    <div className="rect-a6a15098"></div>
                    <div className="rect-7499817c"></div>
                    <div className="rect-e2cb9a08"></div>
                    <div className="rect-378331e0"></div>
                    <div className="svg-372330c0"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
