import React, { useState } from "react";
import clsx from "clsx";
import "../index.css";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  IconButton,
  List,
  Drawer,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Tooltip,
  Box
} from "@material-ui/core";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  Link
} from "react-router-dom";
import {
  Settings,
  Home,
  ChevronLeft,
  ChevronRight,
  RadioButtonUnchecked,
  Menu,
  Storage,
  Info
} from "@material-ui/icons";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
    background: "#1976d2"
    // "#03a9f4"
  },
  drawerPaper: {
    width: drawerWidth,
    background: "#e1f5fe "
  },
  SDClose: {
    color: "#ffffff"
  }
}));

export default function TemporaryDrawer() {
  const classes = useStyles();
  const theme = useTheme();
  const [state, setState] = React.useState({
    left: false
  });

  const [open, setOpen] = useState(false);

  // const [open, setOpen] = React.useState(false);

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom"
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      {/* <div className={classes.toolbar}> */}
      {/* <IconButton onClick={handleDrawerClose}>
          {theme.direction === "rtl" ? (
            <ChevronRightIcon />
          ) : (
            <ChevronLeftIcon />
          )}
        </IconButton> */}
      <div className={classes.drawerHeader}>
        {/* <Typography className={classes.title} variant="h6" noWrap>
          Mobile tracker
        </Typography> */}

        <Box
          pl={1}
          pr={1}
          display="flex"
          justifyContent="space-between"
          width="100%"
          alignItems="left"
        >
          <Tooltip title="Close menu">
            <IconButton
              onClick={handleDrawerClose}
              className={classes.SDClose}
              role="button"
              aria-label="Close side menu"
            >
              {theme.direction === "ltr" ? <ChevronLeft /> : <ChevronRight />}
            </IconButton>
          </Tooltip>
        </Box>
      </div>
      {/* </div> */}

      {/* <List>
        <Divider /> */}

      {/* {["Home", "Wonderwheel", "Summary"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <Home /> : <RadioButtonUnchecked />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))} */}
      {/* </List>
      <Divider /> */}
      {/* <List>
        {["Settings"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{<Settings />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}

        <Link style={{ textDecoration: "none", color: "black" }} to="/">
          <ListItem>
            <IconButton aria-label="Wonderwheel" color="inherit">
              <RadioButtonUnchecked align="left" aria-label="Wonderwheel" />
            </IconButton>
            <ListItemText primary="Wonderwheel" />
          </ListItem>
        </Link> */}
      <List>
        {["Home", "Wonderwheel", "Summary", "About", "Settings"].map(
          (item, index) => (
            <Link className="Link" key={item} to={item}>
              <ListItem button>
                <ListItemIcon>
                  {index === 0 ? (
                    <Home />
                  ) : index === 1 ? (
                    <RadioButtonUnchecked />
                  ) : index === 2 ? (
                    <Storage />
                  ) : index === 3 ? (
                    <Info />
                  ) : (
                    <Settings />
                  )}
                </ListItemIcon>
                <ListItemText primary={item} />
              </ListItem>
            </Link>
          )
        )}
      </List>
      <Divider />
    </div>
  );

  return (
    <div>
      {/* Could probably do something with the iconbutton in the map */}
      {["Menu"].map((anchor) => (
        <React.Fragment key={anchor}>
          <Tooltip title="Open menu">
            <IconButton
              edge="start"
              aria-label="Open menu"
              role="button"
              color="inherit"
              onClick={toggleDrawer(anchor, true)}
            >
              {anchor}
              <Menu
                align="left"
                aria-label="Close menu"
                role="button"
                className={classes.menuButton}
              />
            </IconButton>
          </Tooltip>
          <Drawer
            anchor="left"
            // anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            classes={{
              paper: classes.drawerPaper
            }}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
