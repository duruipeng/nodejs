import { CHANGE_DARK_MODE } from "../constants"; //get constants form constants file

/* DARKMODE REDUCER*/
const calculateDarkMode = () => {
  /* TO CALCULATE SUNRISE / SUNSET BASED ON LOCATION:*/
  var today = new Date();
  var time = today.getHours();
  if (time >= 7 && time <= 18) return false; //if it's between 7am and 6pm, use lightMode, otherwise darkMode
  return true;
};

const initialDarkMode = {
  darkMode: calculateDarkMode() //initial object in the redux store
};

//changeDarkMode function -> use default params (initialState, empty action object)
//reducers get a input of a state and action -> if this one get something we care about (like changingDarkMode), we will do something
export const changeDarkMode = (state = initialDarkMode, action = {}) => {
  switch (action.type) {
    case CHANGE_DARK_MODE: //if a CHANGE_DARK_MODE action comes in, we will do something
      return Object.assign({}, state, { darkMode: action.payload });
    //1st param= new object
    //2nd param= state we receiving
    //3rd param=is what we want to change in the state
    //so what we return is a new object with everything in the state + new darkMode -> 2nd principle: State is read only
    default:
      return state; //if a other action comes in, return the state as it was passed over and do not change anything
  }
};
