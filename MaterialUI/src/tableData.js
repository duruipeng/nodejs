// static table columns
export const tableColumns = [
  {
    title: "ID",
    field: "i_d"
  },

  {
    title: "RGB",
    field: "rgb"
  },

  {
    title: "MAC owner",
    field: "mac_Owner"
  },

  {
    title: "Subnet mask",
    field: "subnet_Mask"
  },

  {
    title: "Hex ID",
    field: "hex"
  }
];

// static table data
export const fakeTableData = [
  {
    i_d: "1",
    rgb: "#07c050",
    hex: "#07c050",
    subnet_Mask: "255255255",
    mac_Owner: "Amazon"
  },
  {
    i_d: "2",
    rgb: "#0c4007",
    hex: "#0c4007",
    subnet_Mask: "255255251",
    mac_Owner: "Huawei"
  },
  {
    i_d: "3",
    rgb: "#0ac0b5",
    hex: "#0ac0b5",
    subnet_Mask: "255255252",
    mac_Owner: "Apple"
  },
  {
    i_d: "4",
    rgb: "#0f803d",
    hex: "#0f803d",
    subnet_Mask: "255255252",
    mac_Owner: "Google"
  },
  {
    i_d: "5",
    rgb: "#068037",
    hex: "#068037",
    subnet_Mask: "255255252",
    mac_Owner: "Unknown"
  }
];
