import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function Collapse() {
  // var inst = new mdui.Collapse('#collapse');

  // // open
  // document.getElementById('open-1').addEventListener('click', function () {
  //   inst.open(0);
  // });
  // document.getElementById('open-2').addEventListener('click', function () {
  //   inst.open('#item-2');
  // });
  // document.getElementById('open-3').addEventListener('click', function () {
  //   inst.open(document.getElementById('item-3'));
  // });

  // // close
  // document.getElementById('close-1').addEventListener('click', function () {
  //   inst.close('#item-1');
  // });
  // document.getElementById('close-2').addEventListener('click', function () {
  //   inst.close(document.getElementById('item-2'));
  // });
  // document.getElementById('close-3').addEventListener('click', function () {
  //   inst.close(2);
  // });

  // // toggle
  // document.getElementById('toggle-1').addEventListener('click', function () {
  //   inst.toggle('#item-1');
  // });
  // document.getElementById('toggle-2').addEventListener('click', function () {
  //   inst.toggle('#item-2');
  // });
  // document.getElementById('toggle-3').addEventListener('click', function () {
  //   inst.toggle('#item-3');
  // });

  // // all
  // document.getElementById('open-all').addEventListener('click', function () {
  //   inst.openAll();
  // });
  // document.getElementById('close-all').addEventListener('click', function () {
  //   inst.closeAll();
  // });

  // // event
  // var item1 = document.getElementById('item-1');
  // item1.addEventListener('open.mdui.collapse', function () {
  //   console.log('item 1 open');
  // });
  // item1.addEventListener('opened.mdui.collapse', function () {
  //   console.log('item 1 opened');
  // });
  // item1.addEventListener('close.mdui.collapse', function () {
  //   console.log('item 1 close');
  // });
  // item1.addEventListener('closed.mdui.collapse', function () {
  //   console.log('item 1 closed');
  // });
  return (
    <div class="mdui-collapse" mdui-collapse>

      <div class="mdui-collapse-item">
        <div class="mdui-collapse-item-header"><p><a href="javascript:;">item1</a></p></div>
        <div class="mdui-collapse-item-body">
          <p>item1 content</p>
          <p>item1 content</p>
          <p>item1 content</p>

          <div class="mdui-collapse" mdui-collapse>
            <div class="mdui-collapse-item">
              <div class="mdui-collapse-item-header"><p><a href="javascript:;">subitem1</a></p></div>
              <div class="mdui-collapse-item-body">
                <p>subitem1 content</p>
                <p>subitem1 content</p>
                <p>subitem1 content</p>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="mdui-collapse-item">
        <div class="mdui-collapse-item-header"><p><a href="javascript:;">item2</a></p></div>
        <div class="mdui-collapse-item-body">
          <p>item2 content</p>
          <p>item2 content</p>
          <p>item2 content</p>
        </div>
      </div>

      <div class="mdui-collapse-item">
        <div class="mdui-collapse-item-header"><p><a href="javascript:;">item3</a></p></div>
        <div class="mdui-collapse-item-body">
          <p>item3 content</p>
          <p>item3 content</p>
          <p>item3 content</p>
        </div>
      </div>

    </div>
  );
}