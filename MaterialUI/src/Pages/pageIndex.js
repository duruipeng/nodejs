import SummaryPage from "./theSummaryPage";
import SettingsPage from "./theSettingsPage";
import HomePage from "./theHomePage";
import WonderWheel from "./theWheelPage";
import ErrorPage from "./theErrorPage";
import AboutPage from "./theAboutPage";

export { AboutPage, ErrorPage, SummaryPage, HomePage };
