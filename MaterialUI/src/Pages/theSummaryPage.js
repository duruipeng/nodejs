import React from "react";
import Summary from "../components/summary";

export default function SummaryPage() {
  return (
    <div>
      <Summary />
    </div>
  );
}
