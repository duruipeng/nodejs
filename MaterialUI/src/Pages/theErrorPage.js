import React from "react";
import Error404 from "../components/error404";

export default function HomePage() {
  return (
    <div>
      <Error404 />
    </div>
  );
}
