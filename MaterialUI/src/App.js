import React from "react";
import "./styles.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  AboutPage,
  HomePage,
  SummaryPage,
  ErrorPage
} from "./Pages/pageIndex.js";
import { Header, Footer } from "./components/index.js";
//    "react": "16.12.0",
//    "react-dom": "16.12.0",
//"react-scripts": "3.0.1",
// const Summary = lazy(() => import("./components/Summary"));
// const Home = lazy(() => import("./components/Home"));

// const mapStateToProps = state => {
//   return {
//     darkMode: state.changeDarkMode.darkMode // -''-
//   };
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     //onSearchChange is a function which will pass the event to it's inner
//     //there the dispatch will get called which will call the setSearchField action in redux and
//     //it will hand over the event.target.value (which is the stuff typed in the search box)
//     onDarkModeChange: darkMode => dispatch(setDarkMode(darkMode))
//   };
// };

// const logger = createLogger();
// const rootReducer = combineReducers({
//   changeDarkMode
// }); //combine the reducers from the reducers file into one root reducer
// const store = createStore(
//   rootReducer,
//   composeWithDevTools(applyMiddleware(thunkMiddleware, logger))
// );
//to create the store with the root reducer and apply the thunkMiddleware and the logger to the store

function App() {
  // const App = () => {

  //  function HowToUse(props) {
  // const [open, setOpen] = React.useState(false);
  // const [, setScroll] = React.useState("paper");

  // const handleClickOpen = scrollType => () => {
  //   setOpen(true);
  //   setScroll(scrollType);
  // };

  // const handleClose = () => {
  //   setOpen(false);
  // };

  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/summary" component={SummaryPage} />
        <Route exact path="/about" component={AboutPage} />
        <Route path="" component={ErrorPage} />
      </Switch>

      <Footer />
    </Router>
  );
}
export default App;
