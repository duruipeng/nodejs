import React, { useEffect, useState } from "react";
// import "./xspreadsheet";
import Spreadsheet from "x-data-spreadsheet";
import "x-data-spreadsheet/dist/xspreadsheet.css";
import "./styles.css";
import localForage from "localforage";
import { xy2expr, expr2xy } from "x-data-spreadsheet/src/core/alphabet";
import { Validations } from "x-data-spreadsheet/src/core/validation";
import Validator from "x-data-spreadsheet/src/core/validator";
import Table from "x-data-spreadsheet/src/component/table";

const defaultSheetData = {};

export default function App() {
  const [defaultData, setDefaultData] = useState("");
  const [instance, setInstance] = useState<any | null>(null);
  const [changeSheetData, setChangeSheetData] = useState<any | null>(null);
  const [rowNum, setRowNum] = useState(0);
  const [column, setRolumn] = useState(0);
  const [currentSelected, setCurrentSelected] = useState(-1);
  const [selectedTemplate, setSelectedTemplate] = useState("");
  const [templateNameText, setTemplateNameText] = useState("");
  const [dialogOpen, setDialogOpen] = useState({
    open: false,
    type: "normal"
  });
  const [templates, setTemplates] = useState<object[]>([]);
  const [showIcon, setShowIcon] = useState<string[]>([]);
  const [locale, setLocale] = useState(new Map());
  const [initialFlg, setInitialFlg] = useState(0);
  const [saveButton, setSaveButton] = useState(false);
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);

  function selectTemplate(index: number, template: string) {
    setCurrentSelected(index);
    setSelectedTemplate(template);
    return;
  }
  function templateClick(template: string) {
    setTemplateNameText(template);
    setDialogOpen({
      open: false,
      type: "テンプレート選択"
    });
    return;
  }
  //登録ボタンの処理
  function handleSave(type) {
    if (instance) {
      console.log("1", instance.datas[0].rows);
      console.log("2", instance.datas[0].clipboard.range);
      console.log("3", instance.datas[0].selector.range);
      console.log(
        "4",
        xy2expr(
          instance.datas[0].clipboard.range.sci,
          instance.datas[0].clipboard.range.sri
        )
      );
      console.log(
        "5",
        xy2expr(
          instance.datas[0].selector.range.sci,
          instance.datas[0].selector.range.sri
        )
      );
      instance.data.rows.copyPaste.call(
        instance.datas[0].rows,
        instance.datas[0].clipboard.range,
        instance.datas[0].selector.range,
        "all"
      );
    }

    //type:1．登録ボタン
    if (type === 1) {
      //入力チェック
      const errMap = inputCheck();
      errMap.forEach(value => {
        value.reverse().forEach(e => {
          console.log(e);
        });
      });
      if (errMap.size === 0) {
        console.log("テンプレートを登録しました");
      }
    }
    return;
  }
  const validate = (
    spreadsheet,
    startri,
    startci,
    endri,
    endci,
    ref,
    validator,
    checkType,
    errMap
  ) => {
    if (!spreadsheet) {
      const errList: any = [];
      errList.push(ref);
      if (errList.length > 0) errMap.set(checkType, errList);
      return false;
    }

    const rows = spreadsheet.rows;
    let rowId = startci;
    while (rows[rowId]) {
      if (rowId > endci) break;
      const cells = rows[rowId].cells;
      let cellId = startri;
      while (cells[cellId]) {
        if (cellId > endri) break;
        const cell = cells[cellId];
        if (checkType === "NotNull") {
          const errList: any = [];
          if (!cell.text) {
            if (errList.indexOf(ref) == -1) {
              console.log(ref + "は入力必須項目です。");
            }
          } else {
            if (cell.merge && cell.merge.length > 0) break;
          }
          if (errList.length > 0) errMap.set(checkType, errList);
        }

        if (checkType === "List") {
          const errList: any = [];
          if (cell.text) {
            if (validator.value.includes(cell.text)) {
              errList.push(
                ref + ":「" + validator.value + "」の値を入力してください。"
              );
            }
          } else {
            if (cell.merge && cell.merge.length > 0) break;
          }
          if (errList.length > 0) errMap.set(checkType, errList);
        }

        if (checkType === "number" || checkType === "date") {
          const errList: any = [];
          let inputValue = null;
          let settingValue;
          //inputValue = parseValue(cell.text, checkType);
        }
      }
    }
  };
  function inputCheck() {
    const validations = instance.datas[0].validations._;
    const errMap = new Map();
    for (let i = 0; i < validations.length; i++) {
      const checkType = validations[i].validator.type;
      for (let j = 0; j < validations[i].refs.length; j++) {
        const cell = validations[i].refs[j].split(":");
        if (cell.length > 1) {
          validate(
            changeSheetData,
            expr2xy(cell[0])[0],
            expr2xy(cell[0])[1],
            expr2xy(cell[1])[0],
            expr2xy(cell[1])[1],
            validations[i].ref[j],
            validations[i].validator,
            checkType,
            errMap
          );
        } else {
          validate(
            changeSheetData,
            expr2xy(cell[0])[0],
            expr2xy(cell[0])[1],
            expr2xy(cell[0])[0],
            expr2xy(cell[0])[1],
            validations[i].ref[j],
            validations[i].validator,
            checkType,
            errMap
          );
        }
      }
    }
    return errMap;
  }
  const settingQuery = async () => {
    const storageData = {
      showIcon: [],
      locale: new Map()
    };
    const savedData: any = await localForage.getItem("IconSetting");
    if (savedData != null) {
      storageData.showIcon = savedData.showIcon;
      storageData.locale = savedData.locale;
      setShowIcon(savedData.showIcon);
      setLocale(savedData.locale);
    } else {
      const list = await getSettings("");
      const icon = list["IconSetting"];
      const showIcons: any = [];
      const locale = new Map();
      icon.forEach(x => {
        if (x.show === "off") {
          showIcons.push(x.name);
        }
        locale.set(x.name, x.locale);
      });
      storageData.showIcon = showIcons;
      storageData.locale = locale;
      setShowIcon(showIcons);
      setLocale(locale);
      await localForage.setItem("IconSetting", storageData);
    }

    setInitialFlg(initialFlg + 1);
    return storageData;
  };

  async function templateQuery() {
    const list = await getTemplates("");
    const templates = list["TemplateList"];
    setTemplates(templates);
    setDialogOpen({
      open: true,
      type: "テンプレート選択"
    });

    return true;
  }

  const mockApi = async (query: string, variables: string) => {
    let response = await fetch("http://localhost:3000/json/" + query + ".json");
    let json = await response.json(); // レスポンスの本文を読み JSON としてパースする
    return json;
  };

  const getSettings = async (contentText: string) => {
    const list = await mockApi("getTemplateSetting", contentText);
    return list;
  };

  const getTemplates = async (contentText: string) => {
    const list = await mockApi("getTemplatList", contentText);
    return list;
  };

  const doShowicon = async () => {
    // 区切線
    const divider = document.getElementsByClassName(
      "x-spreadsheet-toolbar-divider"
    );
    for (let i = 0; i < divider.length; i++) {
      divider[i].setAttribute("style", "display: none"); //非表示
    }
    // 区切線（コンテキストメニュー）
    const dividerContextMenu = document.getElementsByClassName(
      "x-spreadsheet-item divider"
    );
    for (let i = 0; i < dividerContextMenu.length; i++) {
      dividerContextMenu[i].setAttribute("style", "display: none"); //非表示
    }
    // 横並びメニュー
    const normal: any = document.getElementsByClassName(
      "x-spreadsheet-toolbar-btns"
    )[0];
    if (normal) {
      for (let i = 0; i < normal.children.length; i++) {
        if (
          showIcon.includes(normal.children[i].getAttribute("data-tooltip"))
        ) {
          normal.children[i].setAttribute("style", "display: none"); //非表示
        }
      }
    }
  };
  useEffect(() => {
    settingQuery();
    doShowicon();
    const el = document.getElementsByClassName("x-spreadsheet-tooltip")[0];
    if (el && locale.get(el.innerHTML)) {
      document.getElementsByClassName(
        "x-spreadsheet-tooltip"
      )[0].innerHTML = locale.get(el.innerHTML);
    }
    if (instance) {
      setX(instance.sheet.table.data.scroll.x);
      setY(instance.sheet.table.data.scroll.y);
      //console.log(instance)
    }
  }, [initialFlg]);
  useEffect(() => {
    console.log("spreadsheet constructor");
    // new window.x_spreadsheet("#sheet", {
    // const sheet = new XSpreadsheet("#sheet", {
    // @ts-ignore
    const spreadsheet = new Spreadsheet("#sheet", {
      mode: "edit",
      showToolbar: true,
      showGrid: true,
      showContextmenu: true,
      showBottomBar: true,
      view: {
        height: () => document.documentElement.clientHeight,
        width: () => document.documentElement.clientWidth
      },
      row: { len: 100, height: 25 },
      col: {
        len: 52,
        width: 100,
        indexWidth: 50,
        minWidth: 10
      }
    })
      .loadData(defaultSheetData)
      .change(data => {
        if (instance) {
          console.log("instance", instance);
          console.log("data1", data);
          console.log("instance.sheet", instance.sheet.editor.textEl.el);
          // data.rows.forEach((e: any) => {
          //     e;
          // });
          //   if(data.rows[rowNum])
        }
        setDefaultData(JSON.stringify(data));
      })
      .on("cell-selected", (cell, ri, ci) => {
        setRowNum(ri);
        setRolumn(ci);
        console.log("Editor1:", cell);
        console.log("cell:", cell, ", ri:", ri, ", ci:", ci);
      })
      // @ts-ignore
      .on("cell-edited", (text, ri, ci) => {
        setRowNum(ri);
        setRolumn(ci);
        console.log("text:", text, ", ri: ", ri, ", ci:", ci);
      });
    setInstance(spreadsheet);
  }, []);

  function getWithBracket(text) {
    const r = /\[(.+?)\]/g;
    return text.match(r);
  }

  function getInnerBracket(text) {
    const r = /\[(.+?)\]/g;
    return r.exec(text);
  }
  let html = "";
  function setTagValue(value, key, o) {
    const nodes = key.split(".");
    let i = 0;
    let target = null;
    let node;
    while (nodes[i]) {
      if (getWithBracket(nodes[i])) {
        node = nodes[i].split("[")[0];
      } else {
        node = nodes[i];
      }
      if (target) {
        if (Array.isArray(target[node])) {
          if (getWithBracket(nodes[i])) {
            // @ts-ignore
            target = target[node][Number(getInnerBracket(nodes[i])[1])];
          } else {
            target = target[node][0];
          }
        } else {
          target = target[node];
        }
      } else {
        if (Array.isArray(o[node])) {
          if (getWithBracket(nodes[i])) {
            // @ts-ignore
            target = o[node][Number(getInnerBracket(nodes[i])[1])];
          } else {
            target = o[node][0];
          }
        } else {
          target = o[node];
        }
      }
      i++;
      // console.log(i, obj);
      // getField(nodes, obj, 0, null);
      // console.log(i, target);
      if (i === nodes.length - 1) {
        html = html + "<" + nodes[i] + ">##</" + nodes[i] + ">";
        console.log(html);
      }
    }
  }

  function getField(nodes, o, i, value) {
    if (i === nodes.length) return;
    let node;
    if (getWithBracket(nodes[i])) {
      node = nodes[i].split("[")[0];
      const arr = [];
      // @ts-ignore
      // arr[Number(getInnerBracket(nodes[i])[1])] = node;

      arr.push(node);
      o[node] = arr;
    } else {
      node = nodes[i];
      if (o[node]) getField(nodes, o[node], i + 1, value);
      console.log(i, o[node]);
      o[node] =
        i === nodes.length - 1 ? [{ name: node, _text: value }] : [node];
    }
    return nodes.length === i
      ? o[node]
      : getField(nodes, o[node], i + 1, value);
  }

  function getField1(nodes, o, i) {
    o[nodes[i]] = [nodes[i]];
    return nodes.length === i
      ? o[nodes[i]]
      : getField1(nodes, o[nodes[i]], i + 1);
  }

  useEffect(() => {
    if (instance) {
      console.log(instance);
    }

    let obj = {};
    const nodes1 = "Root.Kiji.Sozai[0].aa".split(".");
    getField(nodes1, obj, 0, 123);
    console.log("t", obj);
    const nodes2 = "Root.Kiji.Sozai[1].aa".split(".");
    getField(nodes2, obj, 0, 456);
    const nodes3 = "Root.Kiji.Sozai1[0].aa".split(".");
    getField(nodes3, obj, 0, 789);
    console.log("t", obj);
    // setTagValue('testVal', 'Root.Kiji.Sozai[0].aa', new Object(9));
  }, [x, y]);
  return (
    <>
      <div className="App">
        <input type="button" onClick={() => handleSave(1)} />
        <div id="sheet"></div>
      </div>
      <p style={{ padding: "8px" }}>{defaultData}</p>
    </>
  );
}
