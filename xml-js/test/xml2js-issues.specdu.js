const { value } = require("jsonpath");
var convert = require("../lib");

function getWithBracket(text) {
  const r = /\[(.+?)\]/g;
  return text.match(r);
}

function getInnerBracket(text) {
  const r = /\[(.+?)\]/g;
  return r.exec(text);
}
let html = '';
let obj = {};
function setTagValue(value, key, o) {
  const nodes = key.split(".");
  let i = 0;
  let target = null;
  let node = null;
  while (nodes[i]) {
    if (getWithBracket(nodes[i])) {
      node = nodes[i].split("[")[0];
    } else {
      node = nodes[i];
    }
    if (target) {
      if (Array.isArray(target[node])) {
        if (getWithBracket(nodes[i])) {
          target = target[node][Number(getInnerBracket(nodes[i])[1])];
        } else {
          target = target[node][0];
        }
      } else {
        target = target[node];
      }
    } else {
      if (Array.isArray(o[node])) {
        if (getWithBracket(nodes[i])) {
          target = o[node][Number(getInnerBracket(nodes[i])[1])];
        } else {
          target = o[node][0];
        }
      } else {
        target = o[node];
      }
    }
    i++;
    console.log(i, obj);
    const field = getField(nodes, obj, 0);
    // console.log(i, target);
    if (i === nodes.length - 1) {
      html = html + '<' + nodes[i] + '>##</' + nodes[i] + '>';
      console.log(html);
    }
  }
}

function getField(nodes, o, j) {
  console.log(j, o);
  if (j === 0) {
    o[nodes[j]] = [nodes[j]];
    getField(nodes, o[nodes[j]], j++);
  }
  if (o[nodes[j]]) {
    o[nodes[j]] = [nodes[j]];
    if (nodes[j - 1]) {
      o[nodes[j - 1]] = [o[nodes[j]]];
    }
    return o[nodes[j]]
  }
  return getField(nodes, o[nodes[j]], j++);
}

// describe("Testing xml2js.js:", function () {
//   describe("User reported issues on github:", function () {
//     describe("case by misitoth", function () {
//       setTagValue('testVal', 'Root.Kiji.Sozai[0].aa', new Object(9));
//     });
//   });
// });

describe('case by misitoth', function () {
  // see https://github.com/nashwaan/xml-js/issues/13
  var xml = '<!DOCTYPE svc_init SYSTEM "MLP_SVC_INIT_300.DTD" [<!ENTITY % extension SYSTEM "PIF_EXTENSION_100.DTD">%extension;]>';
  var json = {"_doctype" : "svc_init SYSTEM \"MLP_SVC_INIT_300.DTD\" [<!ENTITY % extension SYSTEM \"PIF_EXTENSION_100.DTD\">%extension;]"};

  it('should output as expected json', function () {
    convert.xml2js(xml, {compact: true});
  });

});


