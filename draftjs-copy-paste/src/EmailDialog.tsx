import React, { useState } from "react";
import {
  createStyles,
  fade,
  Theme,
  withStyles,
  WithStyles
} from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import Button from "@material-ui/core/Button";
import InputBase from "@material-ui/core/InputBase";
import InputLabel from "@material-ui/core/InputLabel";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import CopyToClipboard from "react-copy-to-clipboard";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      width: 558,
      padding: theme.spacing(2)
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500]
    }
  });

const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      "label + &": {
        marginTop: theme.spacing(0)
      },
      width: "100%"
    },
    input: {
      borderRadius: 8,
      position: "relative",
      backgroundColor: theme.palette.common.white,
      border: "1px solid #ced4da",
      fontSize: 16,
      width: "100%",
      padding: "8px 10px",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"'
      ].join(","),
      "&:focus": {
        boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main
      }
    }
  })
)(InputBase);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions);

const EmailDialog = () => {
  const [open, setOpen] = useState(false);
  const [emailContent, setEmailContent] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleContentChange = event => {
    console.log(event.target.value);
    setEmailContent(event.target.value);
  };

  return (
    <div>
      <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
        &nbsp;&nbsp;Open dialog
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Email template for
        </DialogTitle>
        <DialogContent dividers>
          <InputLabel shrink htmlFor="to-input">
            &nbsp;&nbsp;To
          </InputLabel>
          <BootstrapInput defaultValue="" id="to-input" />
          <br />
          <br />
          <InputLabel shrink htmlFor="cc-input">
            &nbsp;&nbsp;cc
          </InputLabel>
          <BootstrapInput defaultValue="" id="cc-input" />
          <br />
          <br />
          <InputLabel shrink htmlFor="subject-input">
            &nbsp;&nbsp;Subject
          </InputLabel>
          <BootstrapInput defaultValue="" fullWidth id="subject-input" />
          <br />
          <br />
          <InputLabel shrink htmlFor="content-input">
            &nbsp;&nbsp;Content
          </InputLabel>
          <BootstrapInput
            multiline
            rows="12"
            defaultValue=""
            id="content-input"
            fullWidth
            onChange={value => handleContentChange(value)}
          />
          <br />
        </DialogContent>
        <DialogActions>
          <CopyToClipboard text={emailContent}>
            <Button autoFocus color="primary">
              Copy content
            </Button>
          </CopyToClipboard>
          {/* <Button onClick={handleClose} color="primary">
            Send email
          </Button> */}
          <Fab
            onClick={handleClose}
            variant="extended"
            size="small"
            color="primary"
            aria-label="add"
          >
            Send email
          </Fab>
        </DialogActions>
      </Dialog>
    </div>
  );
};
export default EmailDialog;
