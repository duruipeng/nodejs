import React from "react";
import ReactDOM from "react-dom";
import EmailDialog from "./EmailDialog";

ReactDOM.render(<EmailDialog />, document.querySelector("#root"));
