import { h } from "./element";
import Button from "./button";
import { bindClickoutside, unbindClickoutside } from "./event";
import { cssPrefix } from "../config";
import { t } from "../locale/locale";

function buildMenu(clsName) {
  return h("div", `${cssPrefix}-item ${clsName}`);
}

function buildSortItem(it) {
  return buildMenu("state")
    .child(t(`sort.${it}`))
    .on("click.stop", () => this.itemClick(it));
}

function buildPullDownBody(items) {
  const { pullDownbEl, pullDownValues } = this;
  pullDownbEl.html("");
  const itemKeys = Object.keys(items);
  itemKeys.forEach((it, index) => {
    const cnt = items[it];
    const active = pullDownValues.includes(it) ? "checked" : "";
    pullDownbEl.child(
      h("div", `${cssPrefix}-item state ${active}`)
        .on("click.stop", () => this.pullDownClick(index, it))
        .children(
          it === "" ? t("pullDown.empty") : it,
          h("div", "label").html(`(${cnt})`)
        )
    );
  });
}

function resetPullDownHeader() {
  const { pullDownhEl, pullDownValues, values } = this;
  pullDownhEl.html(`${pullDownValues.length} / ${values.length}`);
  pullDownhEl.checked(pullDownValues.length === values.length);
}

export default class SortPullDown {
  constructor() {
    this.pullDownbEl = h("div", `${cssPrefix}-body`);
    this.pullDownhEl = h("div", `${cssPrefix}-header state`).on(
      "click.stop",
      () => this.pullDownClick(0, "all")
    );
    this.el = h("div", `${cssPrefix}-sort-pulldown`)
      .children(
        (this.sortAscEl = buildSortItem.call(this, "asc")),
        (this.sortDescEl = buildSortItem.call(this, "desc")),
        buildMenu("divider"),
        h("div", `${cssPrefix}-pulldown`).children(
          this.pullDownhEl,
          this.pullDownbEl
        ),
        h("div", `${cssPrefix}-buttons`).children(
          new Button("cancel").on("click", () => this.btnClick("cancel")),
          new Button("ok", "primary").on("click", () => this.btnClick("ok"))
        )
      )
      .hide();
    // this.setFilters(['test1', 'test2', 'text3']);
    this.ci = null;
    this.sortDesc = null;
    this.values = null;
    this.pullDownValues = [];
  }

  btnClick(it) {
    if (it === "ok") {
      const { ci, sort, pullDownValues } = this;
      if (this.ok) {
        this.ok(ci, sort, "in", pullDownValues);
      }
    }
    this.hide();
  }

  itemClick(it) {
    // console.log('it:', it);
    this.sort = it;
    const { sortAscEl, sortDescEl } = this;
    sortAscEl.checked(it === "asc");
    sortDescEl.checked(it === "desc");
  }

  pullDownClick(index, it) {
    // console.log('index:', index, it);
    const { pullDownbEl, pullDownValues, values } = this;
    const children = pullDownbEl.children();
    if (it === "all") {
      if (children.length === pullDownValues.length) {
        this.pullDownValues = [];
        children.forEach(i => h(i).checked(false));
      } else {
        this.pullDownValues = Array.from(values);
        children.forEach(i => h(i).checked(true));
      }
    } else {
      const checked = h(children[index]).toggle("checked");
      if (checked) {
        pullDownValues.push(it);
      } else {
        pullDownValues.splice(
          pullDownValues.findIndex(i => i === it),
          1
        );
      }
    }
    resetpullDownHeader.call(this);
  }

  // v: autoFilter
  // items: {value: cnt}
  // sort { ci, order }
  set(ci, items, pullDown, sort) {
    this.ci = ci;
    const { sortAscEl, sortDescEl } = this;
    if (sort !== null) {
      this.sort = sort.order;
      sortAscEl.checked(sort.asc());
      sortDescEl.checked(sort.desc());
    } else {
      this.sortDesc = null;
      sortAscEl.checked(false);
      sortDescEl.checked(false);
    }
    // this.setFilters(items, filter);
    this.values = Object.keys(items);
    this.pullDownValues = pullDown
      ? Array.from(pullDown.value)
      : Object.keys(items);
    buildPullDownBody.call(this, items, pullDown);
    resetPullDownHeader.call(this);
  }

  setOffset(v) {
    this.el.offset(v).show();
    let tindex = 1;
    bindClickoutside(this.el, () => {
      if (tindex <= 0) {
        this.hide();
      }
      tindex -= 1;
    });
  }

  show() {
    this.el.show();
  }

  hide() {
    this.el.hide();
    unbindClickoutside(this.el);
  }
}
