import { CellRange } from "./cell_range";
// operator: all|eq|neq|gt|gte|lt|lte|in|be
// value:
//   in => []
//   be => [min, max]
class PullDown {
  constructor(ci, operator, value) {
    this.ci = ci;
    this.operator = operator;
    this.value = value;
  }

  set(operator, value) {
    this.operator = operator;
    this.value = value;
  }

  includes(v) {
    const { operator, value } = this;
    if (operator === "all") {
      return true;
    }
    if (operator === "in") {
      return value.includes(v);
    }
    return false;
  }

  vlength() {
    const { operator, value } = this;
    if (operator === "in") {
      return value.length;
    }
    return 0;
  }

  getData() {
    const { ci, operator, value } = this;
    return { ci, operator, value };
  }
}

class Sort {
  constructor(ci, order) {
    this.ci = ci;
    this.order = order;
  }

  asc() {
    return this.order === "asc";
  }

  desc() {
    return this.order === "desc";
  }
}

export default class AutoPullDown {
  constructor() {
    this.ref = null;
    this.pullDowns = [];
    this.sort = null;
  }

  setData({ ref, pullDowns, sort }) {
    if (ref != null) {
      this.ref = ref;
      this.pullDowns = pullDowns.map(
        it => new PullDown(it.ci, it.operator, it.value)
      );
      if (sort) {
        this.sort = new Sort(sort.ci, sort.order);
      }
    }
  }

  getData() {
    if (this.active()) {
      const { ref, pullDowns, sort } = this;
      return { ref, pullDowns: pullDowns.map(it => it.getData()), sort };
    }
    return {};
  }

  addPullDown(ci, operator, value) {
    const pullDown = this.getPullDown(ci);
    if (pullDown == null) {
      this.pullDowns.push(new PullDown(ci, operator, value));
    } else {
      pullDown.set(operator, value);
    }
  }

  setSort(ci, order) {
    this.sort = order ? new Sort(ci, order) : null;
  }

  includes(ri, ci) {
    if (this.active()) {
      return this.hrange().includes(ri, ci);
    }
    return false;
  }

  getSort(ci) {
    const { sort } = this;
    if (sort && sort.ci === ci) {
      return sort;
    }
    return null;
  }

  getPullDown(ci) {
    const { pullDowns } = this;
    for (let i = 0; i < pullDowns.length; i += 1) {
      if (pullDowns[i].ci === ci) {
        return pullDowns[i];
      }
    }
    return null;
  }

  pullDownRows(getCell) {
    // const ary = [];
    // let lastri = 0;
    const rset = new Set();
    const fset = new Set();
    if (this.active()) {
      const { sri, eri } = this.range();
      const { pullDowns } = this;
      for (let ri = sri + 1; ri <= eri; ri += 1) {
        for (let i = 0; i < pullDowns.length; i += 1) {
          const pullDown = pullDowns[i];
          const cell = getCell(ri, pullDown.ci);
          const ctext = cell ? cell.text : "";
          if (!pullDown.includes(ctext)) {
            rset.add(ri);
            break;
          } else {
            fset.add(ri);
          }
        }
      }
    }
    return { rset, fset };
  }

  items(ci, getCell) {
    const m = {};
    if (this.active()) {
      const { sri, eri } = this.range();
      for (let ri = sri + 1; ri <= eri; ri += 1) {
        const cell = getCell(ri, ci);
        if (cell !== null && !/^\s*$/.test(cell.text)) {
          const key = cell.text;
          const cnt = (m[key] || 0) + 1;
          m[key] = cnt;
        } else {
          m[""] = (m[""] || 0) + 1;
        }
      }
    }
    return m;
  }

  range() {
    return CellRange.valueOf(this.ref);
  }

  hrange() {
    const r = this.range();
    r.eri = r.sri;
    return r;
  }

  clear() {
    this.ref = null;
    this.pullDowns = [];
    this.sort = null;
  }

  active() {
    return this.ref !== null;
  }
}
