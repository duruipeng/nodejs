import React, {useState} from "react";
import { Editor, EditorState, RichUtils, ContentState, SelectionState, Modifier } from "draft-js";

// https://qiita.com/seira/items/f063e262b1d57d7e78b4
class PageContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    // const [editorState, setEditorState] = React.useState(
    //     () => EditorState.createEmpty(),
    //   );
    //debugger
    if (!props) {
      this.state.editorState = EditorState.createWithContent(convertFromRaw(JSON.parse(props.props[11])));
     } else {
      this.state.editorState = EditorState.createEmpty();
      const currentSelection = this.state.editorState.getSelection();
      var tempContentState = this.state.editorState.getCurrentContent();
      var newContentState = ContentState.createFromText("");

      const entityKeyAdded = tempContentState.createEntity('add', 'IMMUTABLE', null).getLastCreatedEntityKey();
      const entityKeyRemoved = tempContentState.createEntity('removed', 'IMMUTABLE', null).getLastCreatedEntityKey();
var aa;
      props.props.forEach((result) => {
          var text = result.props.children.props.children;
          var selectionState = SelectionState.createEmpty(currentSelection.getAnchorKey());

          selectionState = selectionState.merge({
              anchorKey:currentSelection.getAnchorKey(),
              anchorOffset:currentSelection.getAnchorOffset(),
              focusKey:currentSelection.getFocusKey(),
              focusOffset:currentSelection.getFocusOffset(),
              isBackward:false,
              hasFocus: false
          })
          newContentState = Modifier.replaceText(
              tempContentState,
              selectionState,
              text,
              this.state.editorState.getCurrentInlineStyle(),
              result.props.children.props.class === 'add' ? entityKeyAdded : result.props.children.props.class === 'removed' ? entityKeyRemoved : ''
          );
          tempContentState = newContentState;  // 任意のキーが削除された後の EditorState  
      });
      
      aa = EditorState.push(
        this.state.editorState,
        newContentState,
        'change-block-data'
      );
      console.log(aa);
      console.log(aa);
      //setEditorState(newContentState);
      console.log(aa);
     }
     this.dApp();
  }

  dApp() {
    const [value, setValue] = useState(100)
    useEffect(() => {
      setValue(200)
    }, [])
    return (
      <div>
        <h1>Hello Qiita{value}</h1>
      </div>
    );
  }
  onChange = (editorState) => {
    this.setState({
      editorState
    });
  };

  handleKeyCommand = (command) => {
    const newState = RichUtils.handleKeyCommand(
      this.state.editorState,
      command
    );
    if (newState) {
      this.onChange(newState);
      return "handled";
    }
    return "not-handled";
  };

  onUnderlineClick = () => {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, "UNDERLINE")
    );
  };

  onBoldClick = () => {
    this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"));
  };

  onItalicClick = () => {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, "ITALIC")
    );
  };

  render() {
    return (
      <div className="editorContainer">
        <button onClick={this.onUnderlineClick}>U</button>
        <button onClick={this.onBoldClick}>
          <b>B</b>
        </button>
        <button onClick={this.onItalicClick}>
          <em>I</em>
        </button>
        <div className="editors">
          <Editor
            editorState={this.state.editorState}
            handleKeyCommand={this.handleKeyCommand}
            onChange={this.onChange}
          />
        </div>
      </div>
    );
  }
}

export default PageContainer;
