import React, {useState} from "react";
import * as JsDiff from "diff";
import ReactDOM from "react-dom";
import PageContainer from "./components/PageContainer";
import { Editor, EditorState, RichUtils, ContentState, SelectionState, Modifier } from "draft-js";
import $ from 'jquery';
import App from "./App";

import "./styles.css";


const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
