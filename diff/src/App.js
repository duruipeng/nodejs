import React, {useState} from "react";
import * as JsDiff from "diff";
import { Editor, EditorState, RichUtils, ContentState, SelectionState, Modifier, AtomicBlockUtils } from "draft-js";

import './styles.css'

// countの初期値として、1~10までのランダムな数値を生成 
const initialState = Math.floor(Math.random() * 10) + 1

class App extends React.Component {
  constructor(props) {
    super(props);
    //this.componentWillMount();
    const fnMap = {
      chars: JsDiff.diffChars,
      words: JsDiff.diffWords,
      sentences: JsDiff.diffSentences,
      lines: JsDiff.diffLines
    };
    const diff = fnMap['chars'](
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
      { newlineIsToken: true }
    );
    const result = diff.map((part, index) => this.renderResult(part, index));


    var initEditorState1 = EditorState.createEmpty();
    
    var initEditorState = AtomicBlockUtils.insertAtomicBlock(initEditorState1, '', '');
    const currentSelection = initEditorState.getSelection();
    var tempContentState = initEditorState.getCurrentContent();
    var newContentState = ContentState.createFromText("");
    
    const entityKeyAdded = tempContentState.createEntity('add', 'IMMUTABLE', null).getLastCreatedEntityKey();
    const entityKeyRemoved = tempContentState.createEntity('removed', 'IMMUTABLE', null).getLastCreatedEntityKey();
    ;
    //result.forEach((ret) => {
    for(let i=result.length-1; i >= 0; i--) {
        var ret = result[i];
        var text = ret.props.children.props.children;
        var selectionState = SelectionState.createEmpty(currentSelection.getAnchorKey());
    
        selectionState = selectionState.merge({
            anchorKey:currentSelection.getAnchorKey(),
            anchorOffset:0,
            focusKey:currentSelection.getFocusKey(),
            focusOffset:0,
            isBackward:false,
            hasFocus: false
        })
        newContentState = Modifier.replaceText(
            tempContentState,
            selectionState,
            text,
            initEditorState.getCurrentInlineStyle(),
            ret.props.children.props.class === 'add' ? entityKeyAdded : ret.props.children.props.class === 'removed' ? entityKeyRemoved : ''
        );
        console.log(ret.props.children.props.class);
        console.log(newContentState);
        tempContentState = newContentState;  // 任意のキーが削除された後の EditorState  
        initEditorState = EditorState.push(
          initEditorState,
          tempContentState,
          'remove-range'
        );
        this.setState({
          editorState: initEditorState
        });
    }
    //});
    
    
    //setEditorState(newContentState);


    this.state = {
      // クラスでは、コンストラクタ内で、this.stateの初期値{ count: initialState }をセット
        count: initialState,
        // this.stateの初期値{ open: false }をセット
        open: true,
        diffType: 'chars',
        input: result,
        editorState: initEditorState
    }
    console.log(this.state);
  }
  // toggleメソッドを作成 
  toggle = () => {
    this.setState({ open: !this.state.open })
  }


 

  setInput = (v) => {
    this.setState({
      input: v
    });
  };
  setOpen = (v) => {
    this.setState({
      open: v
    });
  };

  renderResult = (part, index) => {
    const spanStyle = {
      backgroundColor: part.added
        ? "salmon"
        : part.removed
        ? "lightgreen"
        : "lightgrey",
      textDecorationLine: part.removed ? "line-through" : "",
      cls: part.added
        ? "add"
        : part.removed
        ? "removed"
        : "",
    };
    return (
      <span key={index}>
        <span style={spanStyle} class={spanStyle.cls}>{part.value}</span>
      </span>
    );
  };
  
  onChange = (editorState) => {
    this.setState({
      editorState
    });
  };

  handleKeyCommand = (command) => {
    const newState = RichUtils.handleKeyCommand(
      this.state.editorState,
      command
    );
    if (newState) {
      this.onChange(newState);
      return "handled";
    }
    return "not-handled";
  };

  onUnderlineClick = () => {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, "UNDERLINE")
    );
  };

  onBoldClick = () => {
    this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"));
  };

  onItalicClick = () => {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, "ITALIC")
    );
  };

  componentWillMount(){
    
  }

  componentDidMount (){
  }
  render() {

    return (
      <>
        <button onClick={this.toggle}>
          {this.state.open ? 'close' : 'open'}
        </button>
        <div className={this.state.open ? 'isOpen' : 'isClose'}>
          <p>現在の数字は {this.state.count} です</p>
          {/*ボタンをクリックした時に、this.setState()を呼ぶことでcountステートを更新 */}
          <button
            onClick={() => this.setState({ count: this.state.count + 1 })}
          >
            + 1
          </button>
          <button
            onClick={() => this.setState({ count: this.state.count - 1 })}
          >
            - 1
          </button>
          <button onClick={() => this.setState({ count: 0 })}>0</button>
          <button onClick={() => this.setState({ count: initialState })}>
            最初の数値に戻す
          </button>
        </div>

        <div className="editorContainer">
            <button onClick={this.onUnderlineClick}>U</button>
            <button onClick={this.onBoldClick}>
              <b>B</b>
            </button>
            <button onClick={this.onItalicClick}>
              <em>I</em>
            </button>
            <div className="editors">
              <Editor
                editorState={this.state.editorState}
                handleKeyCommand={this.handleKeyCommand}
                onChange={this.onChange}
              />
            </div>
          </div>
      </>
    )
  }
}

export default App