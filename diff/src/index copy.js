import React, {useState} from "react";
import * as JsDiff from "diff";
import ReactDOM from "react-dom";
import PageContainer from "./components/PageContainer";
import { Editor, EditorState, RichUtils, ContentState, SelectionState, Modifier } from "draft-js";
import $ from 'jquery';

import "./styles.css";

const fnMap = {
  chars: JsDiff.diffChars,
  words: JsDiff.diffWords,
  sentences: JsDiff.diffSentences,
  lines: JsDiff.diffLines
};
export default class DiffBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty()
    };
    const [editorState, setEditorState] = useState();
    const [result, setResult] = useState();
  }
componentWillMount(){
  // const [editorState, setEditorState] = useState(
  //   () => EditorState.createEmpty(),
  // );

  const diff = fnMap["chars"](
    "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
    "京都府舞鶴市の大聖寺（だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
    { newlineIsToken: true }
  );
//console.log(diff);
  const result = diff.map((part, index) => this.renderResult(part, index));

  this.setInput(result);
  const resultA = diff
    .filter((part) => part.removed || !part.added)
    .map((part, index) => this.renderResult(part, index));

  const resultB = diff
    .filter((part) => part.added || !part.removed)
    .map((part, index) => this.renderResult(part, index));
    //console.log(this.input);
    //console.log(resultA);
    //console.log(resultB);


    this.state.editorState = EditorState.createEmpty();
    const currentSelection = this.state.editorState.getSelection();
    var tempContentState = this.state.editorState.getCurrentContent();
    var newContentState = ContentState.createFromText("");
    
    const entityKeyAdded = tempContentState.createEntity('add', 'IMMUTABLE', null).getLastCreatedEntityKey();
    const entityKeyRemoved = tempContentState.createEntity('removed', 'IMMUTABLE', null).getLastCreatedEntityKey();
    var aa;
    result.forEach((ret) => {
        var text = ret.props.children.props.children;
        var selectionState = SelectionState.createEmpty(currentSelection.getAnchorKey());
    
        selectionState = selectionState.merge({
            anchorKey:currentSelection.getAnchorKey(),
            anchorOffset:currentSelection.getAnchorOffset(),
            focusKey:currentSelection.getFocusKey(),
            focusOffset:currentSelection.getFocusOffset(),
            isBackward:false,
            hasFocus: false
        })
        newContentState = Modifier.replaceText(
            tempContentState,
            selectionState,
            text,
            this.state.editorState.getCurrentInlineStyle(),
            ret.props.children.props.class === 'add' ? entityKeyAdded : ret.props.children.props.class === 'removed' ? entityKeyRemoved : ''
        );
        tempContentState = newContentState;  // 任意のキーが削除された後の EditorState  
    });
    
    aa = EditorState.push(
      this.state.editorState,
      newContentState,
      'change-block-data'
    );
    //setEditorState(newContentState);
    this.setState({
      editorState: aa
    });
    console.log(this.setState.editorState);

    
    this.loadData();        //第一次进入
}
componentDidMount (){
  //alert('componentDidMount');
}

//该组件是作为子组件引用的，并且需要每次进入刷新界面，所以需要用componentWillReceiveProps
//componentWillReceiveProps除了首次调用一次以外，其余都会调用两次
componentWillReceiveProps(nextProps){
    //alert('componentWillReceiveProps');
}
componentWillReceiveProps(nextProps) { // 父组件重传props时就会调用这个方法
  this.setState({someThings: nextProps.someThings});
}
shouldComponentUpdate(nextProps, nextState) {
  //alert('shouldComponentUpdate');
  if(nextProps.someThings === this.props.someThings){
    return false
  }
  return nextState.someData !== this.state.someData
}

async loadData(){
    try{
        //alert('loadData');            //每次请求数据后将num设为2
    }catch (err) {
        //showToast(err.message);
    }
}

handleClick = () => { // 虽然调用了setState ，但state并无变化还是为someThings:1
  alert('handleClick');
  const div1 = document.getElementById('div1');
  let list = document.getElementsByClassName('removed');
  
  //console.log(list);
  for(let i; i<list.length; i++) {alert("fffffffff");
    console.log(list[i]);
    //const aa = list[a] as HTMLElement;
    list[i].className = "2";
//     var firstItem = list[a].firstElementChild;
//list[i].remove();
// var lastItem = text.lastElementChild;
// firstItem.setAttribute('class', 'complete');
// lastItem.setAttribute('class', 'done');
var preview = document.querySelectorAll ("removed");
//console.log(preview);
for (var j = 0; j < preview.length;  j++) {
  preview[j].setAttribute("type", 'href');
}
  }

  const preSomeThings = 6;
   this.setState({
      someThings: preSomeThings
   })
}

  state = {
    // inputA: "test input\nwith multiple\nlines",
    // inputB: "test input\nwith multiple\nlines changed",
    inputA:
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
    inputB:
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
    diffType: "chars",
    editorState: EditorState.createEmpty()
  };

  setType = (e) => {
    this.setState({
      diffType: e.target.value
    });
  };

  setInput = (v) => {
    this.setState({
      input: v
    });
  };
  setInputA = (e) => {
    this.setState({
      inputA: e.target.value
    });
  };

  setInputB = (e) => {
    this.setState({
      inputB: e.target.value
    });
  };

  setInput_0 = (e) => {
    this.setState({
      inputB:
        "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。"
    });
  };

  setInput_1 = (e) => {
    this.setState({
      inputB:
        "1つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。"
    });
  };
  setInput_2 = (e) => {
    this.setState({
      inputB:
        "２つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。"
    });
  };
  setInput_3 = (e) => {
    this.setState({
      inputB:
        "３つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。"
    });
  };
  renderResult = (part, index) => {
    const { diffType } = this.state;
    const spanStyle = {
      backgroundColor: part.added
        ? "salmon"
        : part.removed
        ? "lightgreen"
        : "lightgrey",
      textDecorationLine: part.removed ? "line-through" : "",
      cls: part.added
        ? "add"
        : part.removed
        ? "removed"
        : "",
    };
    return (
      <span key={index}>
        <span style={spanStyle} class={spanStyle.cls}>{part.value}</span>
      </span>
    );
  };

diffState = () => { // 虽然调用了setState ，但state并无变化还是为someThings:1
  const diff = fnMap[this.state.diffType](
    this.state.input,
    this.state.inputA,
    { newlineIsToken: true }
  );
  alert(this.state.input);
  const result = diff.map((part, index) => this.renderResult(part, index));
  this.setInput(result);
}



onChange = (editorState) => {
  this.setState({
    editorState
  });
};

handleKeyCommand = (command) => {
  const newState = RichUtils.handleKeyCommand(
    this.state.editorState,
    command
  );
  if (newState) {
    this.onChange(newState);
    return "handled";
  }
  return "not-handled";
};

onUnderlineClick = () => {
  this.onChange(
    RichUtils.toggleInlineStyle(this.state.editorState, "UNDERLINE")
  );
};

onBoldClick = () => {
  this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"));
};

onItalicClick = () => {
  this.onChange(
    RichUtils.toggleInlineStyle(this.state.editorState, "ITALIC")
  );
};


  render() {

    


    return (
      <div>
        <div className="compbtn">
          <a href="javascript:void(0);" id="diffbtn">
            <div className="text-compbtn" id="diffbtn">
              比較
            </div>
          </a>
        </div>
        <div
          style={{
            whiteSpace: "pre-line",
            width: "100%",
            display: "flex",
            flexDirection: "row",
            alignItems: "top"
          }}
        >
          <select
            style={{ height: 20 }}
            value={this.state.diffType}
            onChange={this.setType}
          >
            <option value="chars">chars</option>
            <option value="words">words</option>
            <option value="sentences">sentences</option>
            <option value="lines">lines</option>
          </select>

          <div
            id="orig"
            style={{
              width: "50%",
              borderRight: "1px solid #dfdfdf",
              border: "1px dashed #000"
            }}
          >
            <hr className="hr1" />
            <ol className="list_test-wrap" id="current">
              <a
                href="javascript:void(0);"
                onClick={this.setInput_0}
                class="link"
                id="0"
              >
                <li id="selected_li">直し登録</li>
              </a>
            </ol>
            <hr className="hr2" />

            <ol className="list_test-wrap">
              <a
                href="javascript:void(0);"
                onClick={this.setInput_1}
                className="link"
                id="1"
              >
                <li className="list_test" id="change1">
                  1つめの編集情報変更
                </li>
              </a>
              <a
                href="javascript:void(0);"
                onClick={this.setInput_2}
                className="link"
                id="2"
              >
                <li className="list_test" id="change2">
                  2つめの編集情報変更
                </li>
              </a>
              {/* <a
                href="javascript:void(0);"
                onClick={this.setInput_3}
                className="link"
                id="3"
              >
                <li className="list_test" id="change3">
                  3つめの編集情報変更
                </li>
              </a> */}
            </ol>
          </div>
          <textarea
            className="Text-0e54d430"
            disabled
            style={{ width: "50%", borderRight: "1px solid #dfdfdf" }}
            id="textarea2"
            rows="15"
            cols="30"
            placeholder="Current version..."
            onChange={this.setInputB}
            value={this.state.inputB}
          />
          <div
            // id="div1"
            id="div1"
            contentEditable="true"
            style={{
              width: "50%",
              borderRight: "1px solid #dfdfdf",
              border: "1px dashed #000"
            }}
            onChange = {this.diffState}
            value={this.state.input}
          >
            {this.state.input}{result}
          <div className="editorContainer">
            <button onClick={this.onUnderlineClick}>U</button>
            <button onClick={this.onBoldClick}>
              <b>B</b>
            </button>
            <button onClick={this.onItalicClick}>
              <em>I</em>
            </button>
            <div className="editors">
              <Editor
                editorState={this.state.editorState}
                handleKeyCommand={this.handleKeyCommand}
                onChange={this.onChange}
              />
            </div>
          </div>
          </div>

          <div onClick = {this.handleClick}>{this.state.someThings}fsdfsdf</div>
          <textarea
            style={{ display: "none" }}
            id="textarea1"
            rows="15"
            cols="30"
            placeholder="Previous version..."
            onChange={this.setInputA}
            value={this.state.inputA}
          />
        </div>

        {/* One block
        <pre className="diff-result">{result}</pre> */}
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        {/* Two blocks
        <div
          style={{
            whiteSpace: "pre-line",
            width: "100%",
            display: "flex",
            flexDirection: "row"
          }}
        >
          <div style={{ paddingLeft: 10, width: "50%" }}>{resultB}</div>
        </div> */}

        <div className="group-51a971ec">
          <div className="rect-254f693c"></div>
          <div className="notifications-24px-04cc0394"></div>
          <div className="svg-382315b8"></div>
          <div className="base-wrapper-19ba19d4">
            <div className="base-088d9368"></div>
          </div>
          <div className="rect-04df0b28"></div>
          <div className="rect-7813e9c4"></div>
          <div className="Text-0dd096a0">
            亡き人へ届いて…「緑のポスト」境内に　舞鶴の大聖寺
          </div>
          <div className="rect-15ea4830"></div>
          <div className="Text-absolute-2c731cec">
            <div className="Text-3122b3f4">
              亡き人へ届いて…「緑のポスト」境内に　舞鶴の大聖寺
            </div>
          </div>
          <div className="Background-acae7060"></div>
          <div className="Text-absolute-5825e568">
            <div className="Text-0e54d430">
              <textarea
                className="Text-0e54d430"
                disabled
                style={{ width: "50%", borderRight: "1px solid #dfdfdf" }}
                id="textarea2"
                rows="15"
                cols="30"
                placeholder="Current version..."
                onChange={this.setInputB}
                value={this.state.inputB}
              />
              {/* <p>
                京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。
              </p> */}
            </div>
          </div>
          <div className="Background-1bcd3b70"></div>
          <div className="Text-absolute-2940ce90">
            <div className="Text-1369605e">
              <div
                id="div2"
                contentEditable="true"
                style={{
                  width: "50%",
                  borderRight: "1px solid #dfdfdf",
                  border: "1px dashed #000"
                }}
              >
                {result}
              </div>
              <textarea
                style={{ display: "none" }}
                id="textarea3"
                rows="15"
                cols="30"
                placeholder="Previous version..."
                onChange={this.setInputA}
                value={this.state.inputA}
              />
              {/* <p></p>
              <p>
                京都府舞鶴市の<span className="Text-span-266c1fba">大聖寺</span>
                （だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。
              </p> */}
            </div>
          </div>
          <div className="rect-ff383ca8"></div>
          <div className="svg-43dbcbb8"></div>
          <div className="svg-5200371c"></div>
          <div className="rect-ca4d05a8"></div>
          <div className="rect-78b768f4"></div>
          <div className="rect-3f38359c"></div>
          <div className="svg-9e1e58a0"></div>
          <div className="text-40022ea0">反映</div>
          <div className="rect-0abd8ee0"></div>
          <div className="text-absolute-642901d6">
            <div className="text-64c7a290">キャンセル</div>
          </div>
          <div className="svg-f42ee1e8"></div>
          <div className="svg-731db7a2"></div>
        </div>
      </div>
    );
  }
}

DiffBox.defaultProps = {
  inputA: "test",
  inputB: "tesowy",
  type: "chars"
};

$(function () {
  var files = { "script.js": "js", "script1.js": "js" };
  static_file(files);
});
function add_static_file(filename, filetype) {
  var fileref;
  if (filetype == "js") {
    fileref = document.createElement("script");
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", filename);
    document.body.appendChild(fileref);
  } else if (filetype == "css") {
    fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    document.getElementsByTagName("head")[0].appendChild(fileref);
  }
}

function remove_static_file(filename, filetype) {
  var target_element =
    filetype == "js" ? "script" : filetype == "css" ? "link" : "none";
  var targetattr =
    filetype == "js" ? "src" : filetype == "css" ? "href" : "none";
  var allsuspects = document.getElementsByTagName(target_element);
  for (var i = allsuspects.length; i >= 0; i--) {
    if (
      allsuspects[i] &&
      allsuspects[i].getAttribute(targetattr) != null &&
      allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1
    )
      allsuspects[i].parentNode.removeChild(allsuspects[i]);
  }
}
function static_file(files) {
  Object.keys(files).forEach(function (key) {
    remove_static_file(key, files[key]);
    add_static_file(key, files[key]);
  });
}
const rootElement = document.getElementById("root");
ReactDOM.render(<DiffBox />, rootElement);
