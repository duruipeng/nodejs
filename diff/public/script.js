var lastEditRange;
$(function () {
  diffjs($("#textarea2").val(), $("#textarea1").val());
});
$("#div1 span span").on("DOMSubtreeModified propertychange", function (e) {
  changevalue();
});
$("#diffbtn").on("click", function () {
  var str = "";
  localStorage.removeItem("lastEditRange");
  $("#div1 span span").each(function (index) {
    if (
      $(this).css("background-color") === "rgb(211, 211, 211)" ||
      $(this).css("background-color") === "rgb(250, 128, 114)"
    ) {
      str = str + $(this).text();
    }
  });
  $("#textarea1").val(str);
  const one = $("#textarea2").val(),
    other = $("#textarea1").val();
  diffjs(one, other);
});
var _c =
  "京都府舞鶴市の大聖寺（だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。";

var _0 =
  "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。";

var _1 =
  "1つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。";

var _2 =
  "２つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。";

var _3 =
  "３つめの編集情報変更京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。";

$(".link").click("DOMSubtreeModified propertychange", function () {
  if (getSelection().rangeCount == 0) {
    var edit = document.getElementById("div1");
    edit.focus();
  }
  let one = $("#textarea2").val(),
    other = $("#textarea1").val();
  if ($(this).attr("id") == 0) {
    one = _0;
  } else if ($(this).attr("id") == 1) {
    one = _1;
  } else if ($(this).attr("id") == 2) {
    one = _2;
  } else if ($(this).attr("id") == 3) {
    one = _3;
  }
  diffjs(one, _c);
});

function diffjs(one, other) {
  let div1 = null;
  let span1 = null;
  let span2 = null;
  const diff = Diff.diffChars(one, other),
    display = $("#div1"),
    fragment = document.createDocumentFragment();
  diff.forEach((part) => {
    const color = part.added
      ? "salmon"
      : part.removed
      ? "lightgreen"
      : "lightgrey";
    span1 = document.createElement("span");
    span2 = document.createElement("span");
    span1.appendChild(span2);
    span2.style.backgroundColor = color;
    if (color === "lightgreen") {
      span2.style.textDecoration = "line-through";
    }
    span2.appendChild(document.createTextNode(part.value));
    fragment.appendChild(span1);
  });
  const fragment_div = document.createDocumentFragment();
  div1 = document.createElement("div");
  div1.id = "div1";
  div1.contentEditable = true;
  div1.style.width = "50%";
  div1.style.border = "1px dashed rgb(0, 0, 0)";
  fragment_div.appendChild(div1);
  div1.appendChild(fragment);
  display.replaceWith(div1);
  $("#div1 span span").bind("DOMSubtreeModified propertychange", function (e) {
    changevalue();
  });
}

function changevalue() {
  var selection = getSelection();
  lastEditRange = selection.getRangeAt(0);
  var node = lastEditRange.startContainer.parentNode;
  $("#div1 span span").each(function (index) {
    if ($(node).css("background-color") === "rgb(144, 238, 144)") {
      var edit = document.getElementById("div1");
      edit.focus();
      if (lastEditRange.endOffset > 0) {
        //console.log("selection:" + lastEditRange.endOffset);
        //console.log(node);
        lastEditRange.setStart(node, 0);
        lastEditRange.setEnd(node, 1);
        selection.deleteFromDocument();
      }
    }
  });
}
