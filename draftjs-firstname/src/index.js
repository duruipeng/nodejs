import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import LinkEditorExample from "./LinkEditorExample";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <React.StrictMode>
    <LinkEditorExample />
    <App />
  </React.StrictMode>,
  rootElement
);
