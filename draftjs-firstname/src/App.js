import React, { useState, useRef, useEffect } from 'react';
import './styles.css';
import {
  CompositeDecorator,
  Editor,
  EditorState,
  convertFromRaw,
  Modifier,
  convertToRaw,
  AtomicBlockUtils,
  ContentState,
  ENTITY_TYPES,
  ENTITY_MUTABILITY,
  SelectionState,
} from 'draft-js';
import {
  Box,
  Button,
  Fade,
  Tooltip,
  Typography,
  withStyles,
} from '@material-ui/core';

const styles = {
  editor: {
    border: '1px solid gray',
    minHeight: '6em',
    fontSize: '32px',
  },
};

const firstNamePlaceholder = '*|user.static_fields.first_name|*';
const firstNamePlaceholderLabel = getText(firstNamePlaceholder);
const text = `This is a Draft-based editor that supports ${firstNamePlaceholderLabel} TeX rendering.`;

const rawContent = {
  blocks: [
    {
      text: text,
      type: 'unstyled',
      entityRanges: [
        {
          offset: text.indexOf(firstNamePlaceholderLabel),
          length: firstNamePlaceholderLabel.length,
          key: 'placehoder_1',
        },
      ],
    },
  ],
  entityMap: {
    placehoder_1: {
      type: 'PLACEHOLDER',
      mutability: 'MUTABLE',
      data: {
        code: firstNamePlaceholder,
      },
    },
  },
};

const content = convertFromRaw(rawContent);

function getText(text) {
  if (text === '*|user.static_fields.first_name|*') {
    return 'Fisrt Name';
  }

  if (text === '*|user.static_fields.last_name|*') {
    return 'Last Name';
  }

  return text;
}

// tooltip
const ProductTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    padding: '8px 16px',
  },

  arrow: {
    '&::before': {
      backgroundColor: theme.palette.primary.main,
    },
  },
}))(Tooltip);
const TokenSpan = ({ children }) => {
  // tooltip
  const [open, setOpen] = useState(true);
  const handlerOpen = () => setOpen(true);
  const handlerClose = () => setOpen(false);

  return (
    <ProductTooltip
      arrow
      open={open}
      interactive
      TransitionComponent={Fade}
      title={
        <div onMouseOver={handlerOpen} onMouseOut={handlerClose}>
          <div>
            <Typography variant="subtitle1">
              Looking for auto-play?
            </Typography>
            <Typography variant="body2">
              Choose whether videos automatically play next.
            </Typography>
          </div>
          <Box display="flex" justifyContent="flex-end">
            <Button color="inherit" onClick={handlerClose}>
              キャンセル
            </Button>
          </Box>
        </div>
      }
    >
      <span
        draggable="true"
        onDragEnd={console.log}
        // contentEditable={false}
      >
        <span
          class={'PLACEHOLDER'}
          text={'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
          onMouseOver={handlerOpen}
          onMouseOut={handlerClose}
        >
          {children}
        </span>
      </span>
    </ProductTooltip>
  );
};
const TokenSpan2 = ({ children }) => {
  return (
    <Tooltip title="Add" aria-label="add">
      <span
        draggable="true"
        onDragEnd={console.log}
        // contentEditable={false}
      >
        <span class={'PLACEHOLDER2'}>{children}</span>
      </span>
    </Tooltip>
  );
};

// const PLACEHOLDER_REGEX = /\*\|(.*?)\|\*/g;

// const findWithRegex = (regex, contentBlock, callback) => {
//   const text = contentBlock.getText();
//   // console.log("text", text);

//   let matchArr, start;
//   while ((matchArr = regex.exec(text)) !== null) {
//     // console.info("matchArr", getText(matchArr[0]));
//     start = matchArr.index;
//     callback(start, start + matchArr[0].length);
//   }
// };

function handleStrategy(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'PLACEHOLDER'
    );
  }, callback);
}
function handleStrategy2(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'PLACEHOLDER2'
    );
  }, callback);
}

const decorator = new CompositeDecorator([
  {
    strategy: handleStrategy,
    component: TokenSpan,
  },
  {
    strategy: handleStrategy2,
    component: TokenSpan2,
  },
]);

// copy paste start
function addExtraData(e) {
  alert(1);
  var clipboardData = e.clipboardData;

  var someObj = {
    test: { test: 'test' },
  };

  var selection = window.getSelection();

  console.log('called addExtraData');
  console.log(selection);

  e.clipboardData.setData('text/plain', selection);
  e.clipboardData.setData('test', JSON.stringify(someObj));

  e.preventDefault();
}

function getExtraData(e) {
  alert(2);
  var data = e.clipboardData.getData('text/plain');
  var data2 = e.clipboardData.getData('test');
  console.log('called getExtraData');
  console.log(data);
  console.log(JSON.parse(data2));
}

document.addEventListener('copy', addExtraData);
document.addEventListener('paste', getExtraData);
// copy paste end

const MyEditor = () => {
  const editorRef = useRef(null);
  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(content, decorator)
  );

  const onChange = nextEditorState => {
    setEditorState(nextEditorState);
  };
  const addFirstNamePink = () => {
    const contentState = editorState.getCurrentContent();
    const targetRange = editorState.getSelection();

    const contentStateWithEntity = contentState.createEntity(
      'PLACEHOLDER',
      'MUTABLE',
      { code: firstNamePlaceholder }
    );

    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newContentState = Modifier.insertText(
      contentState,
      targetRange,
      firstNamePlaceholderLabel,
      undefined,
      entityKey
    );

    setEditorState(
      EditorState.forceSelection(
        EditorState.push(editorState, newContentState),
        newContentState.getSelectionAfter()
      )
    );
  };
  const addFirstNameGray = () => {
    const contentState = editorState.getCurrentContent();
    const targetRange = editorState.getSelection();

    const contentStateWithEntity = contentState.createEntity(
      'PLACEHOLDER2',
      'MUTABLE',
      { code: firstNamePlaceholder }
    );

    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newContentState = Modifier.insertText(
      contentState,
      targetRange,
      firstNamePlaceholderLabel,
      undefined,
      entityKey
    );

    setEditorState(
      EditorState.forceSelection(
        EditorState.push(editorState, newContentState),
        newContentState.getSelectionAfter()
      )
    );
  };

  const logState = () => {
    const currentContent = editorState.getCurrentContent();
    console.log(convertToRaw(currentContent));
  };

  function applyEntity(editorState, entityOptions) {
    let contentState = editorState.getCurrentContent();
    let entityKey = null;
    let contentStateWithEntity = contentState;
    if (entityOptions) {
      let { type, immutable, data } = entityOptions;
      contentStateWithEntity = contentState.createEntity(
        type,
        immutable,
        data
      );
      entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    }
    const newContentState = Modifier.applyEntity(
      contentStateWithEntity,
      editorState.getSelection(),
      entityKey
    );
    console.log('applyEntitynewContentState', newContentState);
    EditorState.push(editorState, newContentState, 'apply-entity');
    console.log(editorState);
    return EditorState.push(
      editorState,
      newContentState,
      'apply-entity'
    );
  }
  // function test() {
  //   let contentState = ContentState.createFromText('I will input the solid state PNG port');
  //   contentState = contentState.createEntity('LINK', 'MUTABLE', {
  //     target: '_blank',
  //     title: 'My link',
  //     url: 'https://en.wikipedia.org/wiki/Portable_Network_Graphics'
  //   });
  //   const contentBlock = contentState.getFirstBlock();
  //   const blockKey = contentBlock.getKey();
  //   let selection = createSelectionState(blockKey, 0, 4);
  //   const linkEntityKey = contentState.getLastCreatedEntityKey();
  //   contentState = Modifier.applyEntity(contentState, selection, linkEntityKey);
  //   contentState = contentState.createEntity(ENTITY_TYPES.image, ENTITY_MUTABILITY.mutable, {
  //     externalUrl: 'my-img.jpg'
  //   });
  //   selection = createSelectionState(blockKey, 8, 14);
  //   const mentionEntityKey = contentState.getLastCreatedEntityKey();
  //   contentState = Modifier.applyEntity(contentState, selection, mentionEntityKey);

  //   contentState = contentState.createEntity(ENTITY_TYPES.link, ENTITY_MUTABILITY.mutable, {
  //     target: '_blank',
  //     title: 'My link',
  //     url: 'https://en.wikipedia.org/wiki/Portable_Network_Graphics'
  //   });
  //   selection = createSelectionState(blockKey, 12, 16);
  //   const linkEntityKey2 = contentState.getLastCreatedEntityKey();
  //   contentState = Modifier.applyEntity(contentState, selection, linkEntityKey2);
  // }
  // function createSelectionState(blockKey, start, end) {
  //   var selectionState = SelectionState.createEmpty(blockKey);
  //   var updatedSelection = selectionState.merge({
  //     focusOffset: start,
  //     anchorOffset:end,
  //   });
  // }
  useEffect(() => {
    const openReviewCommentDialog = e => {
      var clicked = e.target;
      window.removeEventListener(
        'contextmenu',
        openReviewCommentDialog
      );
      if (
        clicked.parentNode &&
        clicked.parentNode.parentNode instanceof HTMLElement
      ) {
        if (
          clicked.parentNode.parentNode.getAttribute('class') ===
          'PLACEHOLDER'
        ) {
          clicked.parentNode.parentNode.setAttribute(
            'class',
            'PLACEHOLDER2'
          );
        }
      }
      const entityOptions = {
        type: 'PLACEHOLDER2',
        immutable: 'MUTABLE',
        data: { code: firstNamePlaceholder },
      };
      applyEntity(editorState, entityOptions);
      //addFirstNameGray();
      // const contentState = editorState.getCurrentContent();
      // const contentStateWithEntity = contentState.createEntity(
      //   'PLACEHOLDER2',
      //   'IMMUTABLE',
      //   { src: 'http://example.com' },
      // );
      // let newEditorState = EditorState.set(editorState, {
      //   currentContent: contentStateWithEntity,
      // });
      // const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
      // newEditorState = AtomicBlockUtils.insertAtomicBlock(
      //   newEditorState,
      //   entityKey,
      //   ' ',
      // );
      // setEditorState(newEditorState);
    };
    document
      .getElementById('root')
      .addEventListener('contextmenu', openReviewCommentDialog);
  }, []);
  useEffect(() => {
    const contentState = editorState.getCurrentContent();
    let entities = [];
    contentState.getBlockMap().forEach(block => {
      // could also use .map() instead
      block.findEntityRanges(character => {
        const charEntity = character.getEntity();
        if (charEntity) {
          // could be `null`
          const contentEntity = contentState.getEntity(charEntity);
          entities.push(contentEntity);
        }
      });
    });

    console.log(
      'getBlockMapgetBlockMap',
      editorState.getCurrentContent().getBlockMap()
    );
    console.log('entitiesentities', entities);
  }, [editorState]);

  const [startText, setStartText] = useState('');
  const [endText, setEndText] = useState('');
  useEffect(() => {
    const mouseupevent = e => {
      if (document.getSelection().rangeCount >= 1) {
        const ranges = document.getSelection().getRangeAt(0);
        window.removeEventListener('mouseup', mouseupevent);

        for (
          let i = 0;
          i < ranges.commonAncestorContainer.childNodes.length;
          i++
        ) {
          for (
            let j = 0;
            j <
            ranges.commonAncestorContainer.childNodes[i].childNodes
              .length;
            j++
          ) {
            const el =
              ranges.commonAncestorContainer.childNodes[i].childNodes[
                j
              ];
            console.log('rangesranges_all', el.insertText);

            if (el && el.getAttribute('class') === 'PLACEHOLDER') {
              el.remove();
            }
          }
        }
      }
    };
    document
      .getElementById('root')
      .addEventListener('mouseup', mouseupevent);
  }, [startText, endText]);

  return (
    <>
      {/* <div style={styles.editor} onClick={() => editorRef.current.focus()}> */}
      <div style={styles.editor}>
        <Editor
          ref={editorRef}
          editorState={editorState}
          onChange={onChange}
        />
      </div>
      <button onClick={addFirstNamePink}>First Name(pink)</button>
      <button onClick={addFirstNameGray}>First Name(gray)</button>
      <button onClick={logState}>Log State</button>
    </>
  );
};

export default function App() {
  return <MyEditor />;
}
