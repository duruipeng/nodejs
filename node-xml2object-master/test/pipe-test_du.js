var assert = require("assert");
var fs = require("fs");
var path = require("path");
var xml2object = require("../lib/xml2object");

describe("Pipe", function () {
  // it('should parse from a piped stream', function(done){
  // 	var stream = fs.createReadStream(
  // 		path.normalize(__dirname + '/fixture/input01_simple.xml'));
  // 	var parser = new xml2object(['dog']);
  // 	var found = [];

  // 	parser.on('object', function(name, obj) {
  // 		found.push(obj.name);
  // 	});

  // 	parser.on('end', function() {
  // 		assert.equal(2, found.length, "Should have found two objects");
  // 		assert.equal('Fluffy', found[0], 'Name mismatch');
  // 		assert.equal('Max', found[1], 'Name mismatch');

  // 		done();
  // 	});

  // 	stream.pipe(parser.saxStream);
  // });

  let obj = {};
  let ancestors = [];
  let current;
  const nodes1 = "Root.Kiji.Sozai[0].aa".split(".");
  getField(nodes1, obj, 0, 123, current);
  console.log("t", obj);
  const nodes2 = "Root.Kiji.Sozai[1].aa.Sozai3[0].bb".split(".");
  getField(nodes2, obj, 0, 456, current);
  const nodes3 = "Root.Kiji.Sozai[2].aa.Sozai3[1].bb".split(".");
  getField(nodes3, obj, 0, 45666, current);
  const nodes4 = "Root.Kiji.Sozai1[0].aa".split(".");
  getField(nodes4, obj, 0, 789, current);
  console.log("t", obj);
});

function getWithBracket(text) {
  const r = /\[(.+?)\]/g;
  return text.match(r);
}

function getInnerBracket(text) {
  const r = /\[(.+?)\]/g;
  return r.exec(text);
}

function getField(nodes, o, i, value, current) {
  if (i === nodes.length) return;
  let node;
  if (getWithBracket(nodes[i])) {
    node = nodes[i].split("[")[0];
    if (i === nodes.length - 1) {
      current = [{ value }];
    } else {
      if (Array.isArray(current)) {
        if (Number(getInnerBracket(nodes[i])[1]) > 0 && current[0][node]) {
          current[0][node].push({});
          current = current[0][node][Number(getInnerBracket(nodes[i])[1])];
        } else {
          current[0][node] = [{}];
          current = current[0][node];
        }
      } else {
        if (current && current[0] && !current[0][node]) {
          if (Array.isArray(current)[0]) {
            current[0][node] = [{}];
            current = current[0][node];
          } else {
            current[node] = [{}];
            current = current[node];
          }
          current = current[0][node];
        } else {
          current[node] = [{}];
        }
      }
    }
  } else {
    node = nodes[i];
    if (i > 0 && getWithBracket(nodes[i - 1])) {
      const e = [{ value }];
      if (current[0] && !current[0][node]) {
        if (i === nodes.length - 1) {
          current[0][node] = e;
        } else {
          current[0][node] = [{}];
        }
        current = current[0][node];
      } else {
        if (Array.isArray(current)) {
          current[0][node] = [{}];
          current = current[0][node];
        } else {
          current[node] = [{}];
          current = current[node];
        }
      }
    } else {
      console.log(i, o);
      if (i === nodes.length - 1) {
        current[0][node] = value;
      } else {
        if (Array.isArray(current)) {
          if (!current[0][node]) {
            current[0][node] = [{}];
          }
          current = current[0][node];
        } else {
          if (!o[node]) {
            o[node] = [{}];
          }
          current = o[node];
        }
      }
    }
  }
  getField(nodes, o, i + 1, value, current);
}
