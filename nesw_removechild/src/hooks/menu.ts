import { Reducer, useReducer } from "react";

export interface menutype {
  id: string;
  title: string;
  url?: string;
  actived?: boolean;
}

function countReducerHandler(state: menutype [], action: string) {
  const index = state.findIndex(e => e.id === action);
  if (index != -1) {
    state[index].actived = !!true
  } else {
    state[0].actived = !!true
  }
  return state
}

export function useCount(menu = [{ id: 's', title: '画面', url: '/', actived: false}]) {
  const [menuDatas, dispatch] = useReducer<Reducer<menutype [], string>>(countReducerHandler, menu)

  const selected = (id: string) => {
    dispatch(id)
  }

  return { menuDatas, selected }
} 