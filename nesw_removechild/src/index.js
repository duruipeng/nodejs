import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import Layout from './layouts/BlankLayout';
import Page from './pages/S301/Views/index';
import reportWebVitals from './reportWebVitals';


import Editor1 from "./editor/Editor1";
import { EditorState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

// // 1
// function Welcome_func(props) {
//   return <h1>Hello, {props.name}</h1>;
// }

// class Welcome_cls extends React.Component {
//   render() {
//     return <h1>Hello, {this.props.name}</h1>;
//   }
// }

// class Welcome_state extends React.Component {
//   render() {
//     return <h1>Hello, {new Date().toLocaleTimeString()}</h1>;
//   }
// }
// setInterval(Welcome_state, 1000);


// // 2
// function Clock(props) {
//   return (
//     <div>
//       <h1>Hello, world!</h1>
//       <h2>It is {props.date.toLocaleTimeString()}.</h2>
//     </div>
//   );
// }

// function tick() {
//    ReactDOM.render(
//     <Clock date={new Date()} />,
//     document.getElementById('root')
//    );
// }

// setInterval(tick, 1000);


const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.secondary
  }
});
class TextEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editorState: EditorState.createEmpty(),
      data: {}
    };
  }

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };
  onEditorStateChange = editorState => {
    console.log(editorState);
    var rawJson = convertToRaw(this.state.editorState.getCurrentContent());
    this.setState({
      editorState,
      data: Object.assign({}, rawJson)
    });
    console.log(this.state.editorState);
  };
  Save = () => {
    console.log(this.state.data);
  };
  
  render() {
    const classes = this.props;

    return (
      <div style={{ padding: "50px" }}>
        <h1> Text Editor</h1>
        <div className={classes.root}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Paper className={classes.paper}>
                <Editor
                  editorState={this.state.editorState}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor"
                  onEditorStateChange={this.onEditorStateChange}
                />
              </Paper>
            </Grid>
            <Grid item xs={6}>
              <Paper className={classes.paper}>
                <div style={{ padding: "10px 0 10px 10px" }}>
                  <h2>Your Daily Times NewsPaper</h2>
                  {this.isEmpty(this.state.data) ? (
                    "Today's Top Headlines"
                  ) : (
                    <Editor1 value={this.state.data} />
                  )}
                </div>
              </Paper>
            </Grid>
          </Grid>
          <div />
          <div />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TextEditor);
ReactDOM.render(
  <React.StrictMode>
  {/* <Welcome_func name="function"/>,
  <Welcome_cls name="class"/>,
  <Welcome_state/>,
    <Page /> */}
    <TextEditor />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
