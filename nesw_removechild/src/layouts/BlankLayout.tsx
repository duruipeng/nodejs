import React from 'react';
import { Inspector } from 'react-dev-inspector';
import 'semantic-ui-css/semantic.min.css'; 
import static_file from '../pages/S301/Views/Static';

const InspectorWrapper = process.env.NODE_ENV === 'development' ? Inspector : React.Fragment;

const Layout: React.FC = ({ children }) => {
  return <InspectorWrapper>{children}</InspectorWrapper>;
};

var files = {'jquery-3.5.1.js':'js',
              'jquery-ui.js':'js',
              'jquery-ui.css':'css'};
static_file(files);
export default Layout;
