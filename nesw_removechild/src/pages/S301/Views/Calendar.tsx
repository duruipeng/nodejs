import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

export default function DatePickers(attr) {
  const classes = useStyles();

  return (
    <TextField
      id={attr.id}
      label=""
      type="text"
      defaultValue= "  年   月   日 ( )"
      className={classes.textField}
      InputLabelProps={{
        shrink: true,
      }}
      required
    />
  );
}
