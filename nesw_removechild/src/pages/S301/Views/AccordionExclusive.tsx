import _ from 'lodash'
import React from 'react'
import { Accordion } from 'semantic-ui-react'
const panels = [
  {
    key: 'first',
    title: '新型コロナウィルス経済対策',
    content: {
      content: (
        <ul>  
        <li><a href="###">GoToキャンペーンは初日の反応</a></li>
        <li><a href="###">GoTo聴取</a></li>
        <li><a href="###">GoTo批判長官会見</a></li>
        <li><a href="###">GoTo予約の手順A</a></li>
        <li><a href="###">GoTo予約の手順B</a></li>
      </ul>
      ),
    },
  },
  {
    key: 'second',
    title: 'パレスチナ問題',
    content: {
      content: (
        <ul>  
        <li><a href="###">GoToキャンペーンは初日の反応</a></li>
        <li><a href="###">GoTo聴取</a></li>
        <li><a href="###">GoTo批判長官会見</a></li>
        <li><a href="###">GoTo予約の手順A</a></li>
        <li><a href="###">GoTo予約の手順B</a></li>
      </ul>
      ),
    },
  },
  {
    key: 'third',
    title: '新型コロナウィルス経済対策',
    content: {
      content: (
        <ul>  
        <li><a href="###">GoToキャンペーンは初日の反応</a></li>
        <li><a href="###">GoTo聴取</a></li>
        <li><a href="###">GoTo批判長官会見</a></li>
        <li><a href="###">GoTo予約の手順A</a></li>
        <li><a href="###">GoTo予約の手順B</a></li>
      </ul>
      ),
    },
  },
]

const AccordionExampleExclusive = () => (
  <Accordion
    defaultActiveIndex={[0, 2]}
    panels={panels}
    exclusive={false}
    fluid
  />
)

export default AccordionExampleExclusive