function add_static_file(filename, filetype) {
	var fileref;
	if (filetype=="js"){
	  fileref=document.createElement('script');
	  fileref.setAttribute("type","text/javascript");
	  fileref.setAttribute("src",filename);
	  document.body.appendChild(fileref);
	}
	else if (filetype=="css"){
	  fileref = document.createElement("link");
	  fileref.setAttribute("rel","stylesheet");
	  fileref.setAttribute("type","text/css");
	  fileref.setAttribute("href",filename);
	  document.getElementsByTagName("head")[0].appendChild(fileref);
	}
  }
  
  function remove_static_file(filename, filetype){
	var target_element=(filetype=="js")? "script" :(filetype=="css")? "link" : "none"
	var targetattr=(filetype=="js")?"src" : (filetype=="css")? "href" :"none"
	var allsuspects : any =document.getElementsByTagName(target_element)
	for (var i=allsuspects.length; i>=0;i--){
	  if (allsuspects[i] &&allsuspects[i].getAttribute(targetattr)!=null && 
		  allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	  allsuspects[i].parentNode.removeChild(allsuspects[i])
	}
  }
  export default function static_file(files) {
	Object.keys(files).forEach(function(key){
	  remove_static_file(key,files[key]);
	  add_static_file(key,files[key]);
	});
  };