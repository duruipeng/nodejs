import React, { Component } from 'react'
import { Accordion, Icon } from 'semantic-ui-react'

export default class AccordionExampleStandard extends Component {
  state = { activeIndex: 0 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { activeIndex } = this.state

    return (
      <Accordion 
      exclusive={false}
      fluid
      >
        <Accordion.Title
          active={activeIndex === 0}
          index={0}
          onClick={this.handleClick}
        >
          <Icon name='dropdown' />
          <span>新型コロナウィルス経済対策</span>
    <i className="angle left icon"></i>
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 0}>
        <ul>  
          <li><a href="###">GoToキャンペーンは初日の反応</a></li>
          <li><a href="###">GoTo聴取</a></li>
          <li><a href="###">GoTo批判長官会見</a></li>
          <li><a href="###">GoTo予約の手順A</a></li>
          <li><a href="###">GoTo予約の手順B</a></li>
        </ul>
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 1}
          index={1}
          onClick={this.handleClick}
        >
          <Icon name='dropdown' />
          <span>パレスチナ問題</span>
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 1}>
          <ul>  
            <li><a href="###">GoToキャンペーンは初日の反応</a></li>
            <li><a href="###">GoTo聴取</a></li>
            <li><a href="###">GoTo批判長官会見</a></li>
            <li><a href="###">GoTo予約の手順A</a></li>
            <li><a href="###">GoTo予約の手順B</a></li>
          </ul>
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 2}
          index={2}
          onClick={this.handleClick}
        >
          <Icon name='dropdown' />
          <span>安倍首相辞任</span>
    <i className="angle left icon"></i>
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 2}>
          <ul>  
            <li><a href="###">GoToキャンペーンは初日の反応</a></li>
            <li><a href="###">GoTo聴取</a></li>
            <li><a href="###">GoTo批判長官会見</a></li>
            <li><a href="###">GoTo予約の手順A</a></li>
            <li><a href="###">GoTo予約の手順B</a></li>
          </ul>
        </Accordion.Content>
      </Accordion>
    )
  }
}
