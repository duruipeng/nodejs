import React from 'react';
import { Tab } from 'semantic-ui-react';
import { Button } from 'semantic-ui-react';
import AccordionExampleExclusive from './AccordionExclusive';
import DraggableDialog from './Dialog';
import TableList from './TableList';

require('./index.less');




const tabContent2 = "tabContent2";
const tabContent3 = "tabContent3";
const tabContent4 = "tabContent4";

const GridExampleRows =<pre><span id="new_icon">NEW</span> 新着テーマ</pre>;
const tabName1 =<a className="item" data-tab="first"><pre><span id="new_icon">NEW</span> 新着テーマ</pre></a>;
const tabName2 = <a className="item" data-tab="second"><i className="search icon"></i>キーワード検索</a>;

const tabContent5 = <div className="ui doubling five column grid">
<div className="column"><i className="lightbulb outline icon"></i>lightbulb outline</div>
<div className="column"><i className="location arrow icon"></i>location arrow</div>
<div className="column"><i className="low vision icon"></i>low vision</div>
<div className="column"><i className="magnet icon"></i>magnet</div>
<div className="column"><i className="male icon"></i>male</div>
<div className="column"><i className="map icon"></i>map</div>
<div className="column"><i className="map outline icon"></i>map outline</div>
<div className="column"><i className="map marker icon"></i>map marker</div>
<div className="column"><i className="map marker alternate icon"></i>map marker alternate</div>
<div className="column"><i className="map pin icon"></i>map pin</div>
<div className="column"><i className="map signs icon"></i>map signs</div>
<div className="column"><i className="medkit icon"></i>medkit</div>
<div className="column"><i className="money bill alternate icon"></i>money bill alternate</div>
<div className="column"><i className="money bill alternate outline icon"></i>money bill alternate outline</div>
<div className="column"><i className="motorcycle icon"></i>motorcycle</div>
<div className="column"><i className="music icon"></i>music</div>
<div className="column"><i className="newspaper icon"></i>newspaper</div>
<div className="column"><i className="newspaper outline icon"></i>newspaper outline</div>
<div className="column"><i className="paw icon"></i>paw</div>
<div className="column"><i className="phone icon"></i>phone</div>
<div className="column"><i className="phone square icon"></i>phone square</div>
<div className="column"><i className="phone volume icon"></i>phone volume</div>
<div className="column"><i className="plane icon"></i>plane</div>
<div className="column"><i className="plug icon"></i>plug</div>
<div className="column"><i className="plus icon"></i>plus</div>
<div className="column"><i className="plus square icon"></i>plus square</div>
<div className="column"><i className="plus square outline icon"></i>plus square outline</div>
<div className="column"><i className="print icon"></i>print</div>
<div className="column"><i className="recycle icon"></i>recycle</div>
<div className="column"><i className="road icon"></i>road</div>
<div className="column"><i className="rocket icon"></i>rocket</div>
<div className="column"><i className="search icon"></i>search</div>
<div className="column"><i className="search minus icon"></i>search minus</div>
<div className="column"><i className="search plus icon"></i>search plus</div>
<div className="column"><i className="ship icon"></i>ship</div>
<div className="column"><i className="shopping bag icon"></i>shopping bag</div>
<div className="column"><i className="shopping basket icon"></i>shopping basket</div>
<div className="column"><i className="shopping cart icon"></i>shopping cart</div>
<div className="column"><i className="shower icon"></i>shower</div>
<div className="column"><i className="street view icon"></i>street view</div>
<div className="column"><i className="subway icon"></i>subway</div>
<div className="column"><i className="suitcase icon"></i>suitcase</div>
<div className="column"><i className="tag icon"></i>tag</div>
<div className="column"><i className="tags icon"></i>tags</div>
<div className="column"><i className="taxi icon"></i>taxi</div>
<div className="column"><i className="thumbtack icon"></i>thumbtack</div>
<div className="column"><i className="ticket alternate icon"></i>ticket alternate</div>
<div className="column"><i className="tint icon"></i>tint</div>
<div className="column"><i className="train icon"></i>train</div>
<div className="column"><i className="tree icon"></i>tree</div>
<div className="column"><i className="trophy icon"></i>trophy</div>
<div className="column"><i className="truck icon"></i>truck</div>
<div className="column"><i className="tty icon"></i>tty</div>
<div className="column"><i className="umbrella icon"></i>umbrella</div>
<div className="column"><i className="university icon"></i>university</div>
<div className="column"><i className="utensil spoon icon"></i>utensil spoon</div>
<div className="column"><i className="utensils icon"></i>utensils</div>
<div className="column"><i className="wheelchair icon"></i>wheelchair</div>
<div className="column"><i className="wifi icon"></i>wifi</div>
<div className="column"><i className="wrench icon"></i>wrench</div>
</div>;
const tabContent6 = <div className="ui doubling five column grid">
<div className="column"><i className="ambulance icon"></i>ambulance</div>
<div className="column"><i className="anchor icon"></i>anchor</div>
<div className="column"><i className="balance scale icon"></i>balance scale</div>
<div className="column"><i className="bath icon"></i>bath</div>
<div className="column"><i className="bed icon"></i>bed</div>
<div className="column"><i className="beer icon"></i>beer</div>
<div className="column"><i className="bell icon"></i>bell</div>
<div className="column"><i className="bell outline icon"></i>bell outline</div>
<div className="column"><i className="bell slash icon"></i>bell slash</div>
<div className="column"><i className="bell slash outline icon"></i>bell slash outline</div>
<div className="column"><i className="bicycle icon"></i>bicycle</div>
<div className="column"><i className="binoculars icon"></i>binoculars</div>
<div className="column"><i className="birthday cake icon"></i>birthday cake</div>
<div className="column"><i className="blind icon"></i>blind</div>
<div className="column"><i className="bomb icon"></i>bomb</div>
<div className="column"><i className="book icon"></i>book</div>
<div className="column"><i className="bookmark icon"></i>bookmark</div>
<div className="column"><i className="bookmark outline icon"></i>bookmark outline</div>
<div className="column"><i className="briefcase icon"></i>briefcase</div>
<div className="column"><i className="building icon"></i>building</div>
<div className="column"><i className="building outline icon"></i>building outline</div>
<div className="column"><i className="car icon"></i>car</div>
<div className="column"><i className="coffee icon"></i>coffee</div>
<div className="column"><i className="crosshairs icon"></i>crosshairs</div>
<div className="column"><i className="dollar sign icon"></i>dollar sign</div>
<div className="column"><i className="eye icon"></i>eye</div>
<div className="column"><i className="eye slash icon"></i>eye slash</div>
<div className="column"><i className="eye slash outline icon"></i>eye slash outline</div>
<div className="column"><i className="fighter jet icon"></i>fighter jet</div>
<div className="column"><i className="fire icon"></i>fire</div>
<div className="column"><i className="fire extinguisher icon"></i>fire extinguisher</div>
<div className="column"><i className="flag icon"></i>flag</div>
<div className="column"><i className="flag outline icon"></i>flag outline</div>
<div className="column"><i className="flag checkered icon"></i>flag checkered</div>
<div className="column"><i className="flask icon"></i>flask</div>
<div className="column"><i className="gamepad icon"></i>gamepad</div>
<div className="column"><i className="gavel icon"></i>gavel</div>
<div className="column"><i className="gift icon"></i>gift</div>
<div className="column"><i className="glass martini icon"></i>glass martini</div>
<div className="column"><i className="globe icon"></i>globe</div>
<div className="column"><i className="graduation cap icon"></i>graduation cap</div>
<div className="column"><i className="h square icon"></i>h square</div>
<div className="column"><i className="heart icon"></i>heart</div>
<div className="column"><i className="heart outline icon"></i>heart outline</div>
<div className="column"><i className="heartbeat icon"></i>heartbeat</div>
<div className="column"><i className="home icon"></i>home</div>
<div className="column"><i className="hospital icon"></i>hospital</div>
<div className="column"><i className="hospital outline icon"></i>hospital outline</div>
<div className="column"><i className="image icon"></i>image</div>
<div className="column"><i className="image outline icon"></i>image outline</div>
<div className="column"><i className="images icon"></i>images</div>
<div className="column"><i className="images outline icon"></i>images outline</div>
<div className="column"><i className="industry icon"></i>industry</div>
<div className="column"><i className="info icon"></i>info</div>
<div className="column"><i className="info circle icon"></i>info circle</div>
<div className="column"><i className="key icon"></i>key</div>
<div className="column"><i className="leaf icon"></i>leaf</div>
<div className="column"><i className="lemon icon"></i>lemon</div>
<div className="column"><i className="lemon outline icon"></i>lemon outline</div>
<div className="column"><i className="life ring icon"></i>life ring</div>
<div className="column"><i className="life ring outline icon"></i>life ring outline</div>
<div className="column"><i className="lightbulb icon"></i>lightbulb</div>
</div>;




const panes = [
  { menuItem: '新着テーマ', render: () => <Tab.Pane>{tabContent1}</Tab.Pane> },
  { menuItem: 'キーワード検索', render: () => <Tab.Pane>{tabContent2}</Tab.Pane> },
  { menuItem: 'ピン止め', render: () => <Tab.Pane>{tabContent3}</Tab.Pane> },
  { menuItem: '条件１', render: () => <Tab.Pane>{tabContent4}</Tab.Pane> },
  { menuItem: '条件２', render: () => <Tab.Pane>{tabContent5}</Tab.Pane> },
  { menuItem: '＋', render: () => <Tab.Pane>{tabContent6}</Tab.Pane> }
]




const tabContent1 = <div className="tab_container">
<Button primary>政治部</Button><Button>社会部</Button><Button primary>社会部</Button><Button>＋　追加</Button><DraggableDialog/>
<div className="menu_title">
<span className="menu_title_name">更新があったテーマ一覧</span>
<div className="menu_scrollbar">


<div className="ui styled fluid accordion">
<AccordionExampleExclusive />
</div>


  </div>
  </div>
<div className="content_list"><span>新型コロナウィルス経済対策</span><i className="thumbtack icon"></i>
  </div>

<TableList/>
</div>;

export default function Page() {
  // Modal
  return (
    <div>
    <Tab panes={panes}></Tab>
    </div>
  );
}
