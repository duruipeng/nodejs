import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { Select, TextArea } from 'semantic-ui-react';
import Checkbox from '@material-ui/core/Checkbox';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import DatePickers from './Calendar';
import TimePickers from './Time';
import static_file from './Static';
import $ from 'jquery';
import { Accordion } from 'semantic-ui-react'


const row =  {title : "4", memo : "4", theme : "4"};
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

function PaperComponent2(props) {
  return (
    <Draggable handle="#draggable-dialog-title2" cancel={'[class*="MuiDialogContent-root2"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const countryOptions = [
  { key: 'ae', value: 'ae', text: '' },
  { key: 'af', value: 'af', text: '繰り返しなし' },
  { key: 'ax', value: 'ax', text: '繰り返しあり' },
]

const panels = [
  {
    key: 'first',
    title: '取材予定',
    content: {
      content: (
        <div>
        <div className="edit_title_label">お彼岸特集インタビュー</div>
        <hr className="dialog_hr"/><br/>
        <div className="start">
          <div className="start_label">開始日時</div>
          <div className="start_date"><DatePickers id="start_date" /><div id="start_date_icon"></div></div>
          <span className="start_date_year"/>
          <span className="start_date_month"/>
          <span className="start_date_day"/>
          <div className="start_time"><TimePickers id="start_time"/></div>
          <Checkbox 
                  color="primary"
                  inputProps={{ 'aria-label': 'checkbox with default color' }}/>
          <div className="start_setting">日時未定</div>
          <Checkbox 
                  color="primary"
                  inputProps={{ 'aria-label': 'checkbox with default color' }}/>
          <div className="start_oneday">終日</div>
          <Select placeholder='' options={countryOptions} name="duplicate" id="duplicate"/>
        </div><br/>
        <div className="end">
          <div className="end_label">終了日時</div>
          <div className="end_date"><DatePickers id="end_date" /><div id="start_date_icon"></div></div>
          <span className="end_date_year"/>
          <span className="end_date_month"/>
          <span className="end_date_day"/>
          <div className="end_time"><TimePickers id="end_time" /></div>
          <Checkbox 
                  color="primary"
                  inputProps={{ 'aria-label': 'checkbox with default color' }}/>
          <div className="end_setting">指定なし</div>
        </div>
        <br/>
        <div className="target">
          <div className="target_label">取材対象</div>
          <div className="target_area"><TextArea id="target_area" name="target_area" placeholder='取材の対象、取材メモ' /></div>
        </div>
        </div>
      ),
    },
  },
  {
    key: 'second',
    title: 'テーマ',
    content: {
      content: (
        "テーマの内容欄"
      ),
    },
  },
]
export default function DraggableDialog() {
  $(function () {
    var files = { 'customize.js':'js' };
    static_file(files);
  });

  // button1
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleOpenNew = () => {
    setOpen(true);
  };

  // button2
  const [open2, setOpen2] = React.useState(false);

  const handleClickOpen2 = () => {
    setOpen2(true);
  };

  const handleClose2 = () => {
    setOpen2(false);
  };
  const handleOpenNew2 = () => {
    setOpen2(true);
  };
  return (
    <div><div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen} id="new">
        ＋　新規登録
      </Button>
      <Button variant="outlined" color="primary" id="delete">
        ―　削　　除
      </Button>
      </div>

{/* dialog1 */}
      <Dialog 
        id="dialog1"
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Subscribe
          <Button variant="outlined" color="primary" onClick={handleClickOpen2}>
            ・・・
          </Button>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>



            <Accordion
              defaultActiveIndex={[0, 1]}
              panels={panels}
              exclusive={false}
              fluid
            />


          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOpenNew} color="primary" id="handleOpenNew">
            登録
          </Button>
        </DialogActions>
      </Dialog>






{/* dialog2 */}
          <Dialog
            id="dialog2"
            open={open2}
            onClose={handleClose2}
            PaperComponent={PaperComponent2}
            aria-labelledby="draggable-dialog-title2"
          >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title2">
              コメント
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
              <div className="comment_main">
                <div className="system_date"><span>昨日</span></div>
                <div className="system_time"><span>00:00</span></div>
                <div className="system_info"><span>高橋陽介さん　取材予定を登録しました。</span></div>
                <br/><br/><br/>
                <div className="comment">
                  <div className="comment_head">
                      <div className="comment_avatar"><span>佐</span></div>
                      <div className="comment_name">高橋陽介</div>
                  </div>
                  <div className="comment_content">
                    <div className="comment_mailto"><a>@高橋陽介</a></div>
                    <div className="comment_word"><span>配信用の会場風景撮影もお願いします。</span></div>
                  </div>
                  <br/>
                  <div className="comment_foot">
                    <div className="comment_like"><span>^o^</span></div>
                    <div className="comment_time"><span>00:00</span></div>
                  </div>
                </div>
                <hr className="comment_hr"/><br/>

                <div className="system_date">今日</div>
                <div className="system_info"></div>
                <br/><br/>
                <div className="comment">
                  <div className="comment_head_self">
                      <div className="comment_avatar"><span>高</span></div>
                      <div className="comment_name"></div>
                  </div>
                  <div className="comment_content">
                    <div className="comment_mailto"><a>@佐藤太郎</a></div>
                    <div className="comment_word">承知しました。</div>
                  </div>
                  <div className="comment_foot">
                    <div className="comment_like">^o^</div>
                    <div className="comment_time">00:00</div>
                  </div>
                </div>
                <br/><br/>
                <hr className="comment_hr"/><br/>
              </div>

              </DialogContentText>
            </DialogContent>
            <DialogActions>
                <div className="comment_send">
                  <div className="send_word">^o^</div>
                  <div className="send_attach">00:00</div>
                  <div className="send_button">00:00</div>
                </div>
              <Button onClick={handleOpenNew2} color="primary">
                発送
              </Button>
            </DialogActions>
          </Dialog>

    </div>
  );
}
