import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import myInitObject from './Data';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(title, memo, theme, start_datetime, end_datetime) {
  return { title, memo, theme , start_datetime, end_datetime};
}

var table_list = new Array();
var ls : any = localStorage.getItem('tableList');
table_list = JSON.parse(ls);
if (table_list==null) {
  localStorage.setItem('tableList',JSON.stringify(myInitObject.tableList));
}
let rows = new Array();
for (var i in table_list) {
    if(table_list[i] != null){
      if(table_list[i].title == null) table_list[i].title = "";
      if(table_list[i].memo == null) table_list[i].memo = "";
      if(table_list[i].theme == null) table_list[i].theme = "";
      if(table_list[i].start_datetime == null) table_list[i].start_datetime = "";
      if(table_list[i].end_datetime == null) table_list[i].end_datetime = "";
      rows[i] = createData(table_list[i].title, table_list[i].memo, table_list[i].theme, table_list[i].start_datetime, table_list[i].end_datetime);
    } 
}

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="right">目出し</TableCell>
            <TableCell align="right">取材メモ</TableCell>
            <TableCell align="right">テーマ</TableCell>
            <TableCell align="right">開始日時</TableCell>
            <TableCell align="right">最終日時</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.title}>
              <TableCell component="th" scope="row">
                {row.title}
              </TableCell>
              <TableCell align="right">{row.memo}</TableCell>
              <TableCell align="right">{row.theme}</TableCell>
              <TableCell align="right">{row.start_datetime}</TableCell>
              <TableCell align="right">{row.end_datetime}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}