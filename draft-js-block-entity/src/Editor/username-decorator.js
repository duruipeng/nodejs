import React, {Component} from 'react';
import {CompositeDecorator} from "draft-js";
const USERNAME_REGEX = /\@[\w]+/g;

export default new CompositeDecorator([
  {
    strategy(contentBlock, callback) {
      const text = contentBlock.getText();
      let matchArr, start;
      while ((matchArr = USERNAME_REGEX.exec(text)) !== null) {
        start = matchArr.index;
        callback(start, start + matchArr[0].length);
      }
    },
    component(props) {
      return <span {...props} style={styles.handle}>{props.children}</span>;
    },
  },
]);

const styles = {
  handle: {
    color: "rgba(98, 177, 254, 1.0)",
    direction: "ltr",
    unicodeBidi: "bidi-override",
  }
};