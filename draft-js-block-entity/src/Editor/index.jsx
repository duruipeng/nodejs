import React, {Component} from 'react';
import * as JsDiff from "diff";
import { Editor, EditorState, RichUtils, ContentState, SelectionState, Modifier, AtomicBlockUtils, convertToRaw, CompositeDecorator, Entity } from "draft-js";
import {Map} from 'immutable';

import {InlineStyleControls, styleMap} from './InlineStyleControls';
import {BlockStyleControls} from './BlockStyleControls';
import {block} from 'bem-cn';

import './index.css'
import {FireBlock} from './FireBlock';
import {Poll} from '../Poll';
import {PollModal} from '../PollModal';
import decorator1 from "./username-decorator";

const b = block('editor');

export default class extends Component {
  constructor(props) {
    super(props);



    const fnMap = {
      chars: JsDiff.diffChars,
      words: JsDiff.diffWords,
      sentences: JsDiff.diffSentences,
      lines: JsDiff.diffLines
    };
    const diff = fnMap['chars'](
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「緑のポスト」。６月に設置して亡き人への手紙を受けつけ、３カ月ほどで１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
      "京都府舞鶴市の大聖寺（だいしょうじ）にある「みどりのポスト」。６月に設置して亡き人への手紙を受付け、３カ月程で１２０通余りが寄せられた。秋のお彼岸を迎え、同寺で２３日、亡くなった人に思いを届けようと、祈願供養と手紙のお焚（た）きあげがあった。",
      { newlineIsToken: true }
    );
    const result = diff.map((part, index) => this.renderResult(part, index));

    const handleStrategy = (contentBlock, callback, contentState) => {
      findWithRegex(HANDLE_REGEX, contentBlock, callback);
    };
    
    const hashtagStrategy = (contentBlock, callback, contentState) => {
      findWithRegex(HASHTAG_REGEX, contentBlock, callback);
    };
    
    const findWithRegex = (regex, contentBlock, callback) => {
      const text = contentBlock.getText();
      let matchArr, start;
      while ((matchArr = regex.exec(text)) !== null) {
        start = matchArr.index;
        callback(start, start + matchArr[0].length);
      }
    };
    const decorator = new CompositeDecorator([
      {
        strategy: handleStrategy,
        component: HandleSpan,
      },
      {
        strategy: hashtagStrategy,
        component: HashtagSpan,
      },
      {
        strategy: diffStrategy,
        component: diffSpan,
      },
    ]);
    var initEditorState = EditorState.createEmpty(decorator);
    
    //var initEditorState = AtomicBlockUtils.insertAtomicBlock(initEditorState1, '', '');
    const currentSelection = initEditorState.getSelection();
    var tempContentState = initEditorState.getCurrentContent();
    var newContentState = ContentState.createFromText("");
    

    for(let i=result.length-1; i >= 0; i--) {
        const entityKeyAdded = tempContentState.createEntity('add', 'IMMUTABLE', null).getLastCreatedEntityKey();
        const entityKeyRemoved = tempContentState.createEntity('removed', 'IMMUTABLE', null).getLastCreatedEntityKey();
        var ret = result[i];
        var text = ret.props.children.props.children;
        var selectionState = SelectionState.createEmpty(currentSelection.getAnchorKey());
    
        selectionState = selectionState.merge({
            anchorKey:currentSelection.getAnchorKey(),
            anchorOffset:currentSelection.getAnchorOffset(),
            focusKey:currentSelection.getFocusKey(),
            focusOffset:currentSelection.getFocusOffset(),
            isBackward:true,
            hasFocus: false
        })

        newContentState = Modifier.replaceText(
            tempContentState,
            selectionState,
            text,
            initEditorState.getCurrentInlineStyle(),
            ret.props.children.props.class === 'add' ? entityKeyAdded : ret.props.children.props.class === 'removed' ? entityKeyRemoved : ''
        );
        tempContentState = newContentState;  // 任意のキーが削除された後の EditorState 
        // var aa = AtomicBlockUtils.insertAtomicBlock(aa, entityKeyAdded, '');
        // aa = AtomicBlockUtils.insertAtomicBlock(aa, entityKeyAdded, '');
        // aa = AtomicBlockUtils.insertAtomicBlock(aa, entityKeyAdded, '');
      console.log('newContentStatenewContentState', tempContentState);
    } 
    console.log('resultresult', result);
    initEditorState = EditorState.push(
      initEditorState,
      newContentState,
      'apply-entity'
    );

    const contentStateWithEntity = initEditorState.getCurrentContent().createEntity('LINK', 'MUTABLE', {
      url: 'http://www.zombo.com',
    });
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const contentStateWithLink = Modifier.applyEntity(
      contentStateWithEntity,
      initEditorState.getSelection(),
      entityKey,
    );
    const newEditorState = EditorState.set(initEditorState, {
      currentContent: contentStateWithLink,
    });
    console.log(newEditorState);
    this.setState({
      editorState: newEditorState
    });
    
    this.state = {
      editorState: newEditorState,
      isOpen: false,
      open: true,
      diffType: 'chars',
      input: result
    };
    // eslint-disable-next-line react/no-string-refs
    this.focus = () => this.refs.editor.focus();
  }

  renderResult = (part, index) => {
    const spanStyle = {
      backgroundColor: part.added
        ? "salmon"
        : part.removed
        ? "lightgreen"
        : "lightgrey",
      textDecorationLine: part.removed ? "line-through" : "",
      cls: part.added
        ? "add"
        : part.removed
        ? "removed"
        : "",
    };
    return (
      <span key={index}>
        <span style={spanStyle} class={spanStyle.cls}>{part.value}</span>
      </span>
    );
  };



  onChange = editorState => {
    console.log(
      'content',
      editorState.getBlockTree(editorState.getSelection().getStartKey())
    );
    this.setState({editorState}, () => {
      //setTimeout(() => this.focus(), 0);
    });
  };

  openPollModal = () => {
    this.setState({isOpen: true})
  };

  closePollModal = () => {
    this.setState({isOpen: false})
  };

  togglePoll = (data) => {
    let newEditorState;
    const contentState = this.state.editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'poll',
      'IMMUTABLE',
      {data}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    console.log('entk', entityKey);
    newEditorState = EditorState.set(
      this.state.editorState,
      {currentContent: contentStateWithEntity}
    );
    newEditorState = RichUtils.toggleBlockType(newEditorState, 'poll-block');

    this.onChange(RichUtils.insertSoftNewline(newEditorState));
    this.closePollModal();
  };

  toggleBlockType = blockType => {
    if (blockType === 'poll-block') {
      const startKey = this.state.editorState.getSelection().getStartKey();
      const selectedBlockType = this.state.editorState
        .getCurrentContent()
        .getBlockForKey(startKey)
        .getType();
      console.log(selectedBlockType)
      if (selectedBlockType === 'poll-block') {
        this.onChange(
          RichUtils.toggleBlockType(this.state.editorState, blockType)
        );
        return;
      }
      this.openPollModal();
      return;
    }
    this.onChange(
      RichUtils.toggleBlockType(this.state.editorState, blockType)
    );
  };

  toggleInlineStyle = inlineStyle => {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, inlineStyle)
    );
  };

  render() {
    const {editorState} = this.state;

    const contentState = editorState.getCurrentContent();

    return (
      <div className={b({'placeholder': !contentState.hasText()})}>
        <PollModal
          onSubmit={this.togglePoll}
          isOpen={this.state.isOpen}
          onClose={this.closePollModal}
          onSubmit={this.togglePoll}
        />

        <div className={b('panel')}>
          <BlockStyleControls
            editorState={editorState}
            onToggle={this.toggleBlockType}
          />
          <InlineStyleControls
            editorState={editorState}
            onToggle={this.toggleInlineStyle}
          />
        </div>
        <div className={b('container')} onClick={this.focus}>
          <Editor
            blockRendererFn={getBlockRender}
            blockStyleFn={getBlockStyle}
            customStyleMap={styleMap}
            editorState={editorState}
            onChange={this.onChange}
            placeholder="Tell your story ✍🏻..."
            ref="editor"
            //blockRenderMap={extendedBlockRenderMap}
          />
        </div>
      </div>
    );
  }
}

function getBlockStyle(block) {
  // switch (block.getType()) {
  //   case 'code-block':
  //     return 'code-block';
  //   default:
  //     return null;
  // }
  const type = block.getType();
  if (type === 'unstyled') {
    return 'superFancyBlockquote';
  }
}

function getBlockRender(block) {
  switch (block.getType()) {
    case 'blockquote':
      return 'RichEditor-blockquote';
    case 'fire-block':
      return {
        component: FireBlock,
        editable: false,
    };
    case 'poll-block':
      return {
        component: Poll,
        editable: false,
      };
    default:
      return null;
  }
}

const HANDLE_REGEX = /@[\w]+/g;
const HASHTAG_REGEX = /#[\w\u0590-\u05ff]+/g;

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}
function diffStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

function findWithRegex(regex, contentBlock, callback) {
  const text = contentBlock.getText();
  let matchArr, start;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

const HandleSpan = (props) => {
  return (
    <span
      style={styles.handle}
      data-offset-key={props.offsetKey}
    >
      {props.children}
    </span>
  );
};

const HashtagSpan = (props) => {
  return (
    <span
      style={styles.hashtag}
      data-offset-key={props.offsetKey}
    >
      {props.children}
    </span>
  );
};

const diffSpan = (props) => {
  return (
    <span
      style={styles.add}
      data-offset-key={props.offsetKey}
    >
      {props.children}
    </span>
  );
};

const styles = {
  root: {
    fontFamily: '\'Helvetica\', sans-serif',
    padding: 20,
    width: 600,
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10,
  },
  button: {
    marginTop: 10,
    textAlign: 'center',
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)',
  },
  add: {
    color: 'rgba(95, 184, 138, 1.0)',
  },
  removed: {
    color: 'rgba(95, 184, 138, 1.0)',
  },
};


/*
const blockRenderMap = Map({
  'fire-block': {
    element: FireBlock,
  },
  'poll-block': {
    element: Poll,
  },
});
const extendedBlockRenderMap = DefaultDraftBlockRenderMap.merge(
  blockRenderMap
);
*/
